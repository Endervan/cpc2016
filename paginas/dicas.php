<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-dicas -->
  <!--  ==============================================================  -->
  <div class="container-fluid">
   <div class="row">
    <div class="container titulo-internos">
      <div class="row ">
        <div class="col-xs-5 top60 text-right">
          <h1>CONFIRA NOSSAS</h1>
          <h2>DICAS</h2>

          <!-- barra pesquisas -->
          <form action="<?php echo Util::caminho_projeto() ?>/dicas/" method="post">
            <div class="input-group stylish-input-group top20">
              <input type="text" class="form-control" name="busca_equipamentos" placeholder="PESQUISAR" >
              <span class="input-group-addon">
                <button type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>  
              </span>
            </div>
          </form>
          <!-- barra pesquisas -->

        </div>
      </div>
    </div> 
  </div> 
</div>
<!--  ==============================================================  -->
<!-- bg-dicas -->
<!--  ==============================================================  -->

<!--  ==============================================================  -->
<!-- DICAS HOME-->
<!--  ==============================================================  -->
<div class="container-fluid fundo-cinza pb40">
  <div class="row">
    <div class="container subir-dicas">
      <div class="row ">
        

        
        <!--  ==============================================================  -->
        <!-- item 01-->
        <!--  ==============================================================  -->
        <?php
        if(isset($_POST[busca_equipamentos])):
          $complemento = "and titulo LIKE '%$_POST[busca_equipamentos]%'";
        endif;

        $result = $obj_site->select("tb_dicas", $complemento);
         if(mysql_num_rows($result) == 0){
            echo '
                  <h6 class="top80">
                    Nenhum registro encontrado.
                  </h6>
                  ';
         }else{ 
           $i = 0;
           while ($row = mysql_fetch_array($result)) {
           ?>
              <!-- item 01 -->
              <div class="col-xs-4 text-center">
                <div class="thumbnail dicas-home">
                  <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 358, 327, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                    <div class="borda-interna"></div>
                  </a>
                  <div class="caption">
                    <div class="top20">
                      <p><?php Util::imprime($row[titulo]); ?></p>
                    </div>
                    <a class="btn btn-transparente-conheca top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>">
                      SAIBA MAIS
                    </a>
                  </div>
                </div>
              </div>
            <?php 
            if ($i == 2) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }
            
          }
        }
        ?>




      </div>
    </div> 
  </div>
</div>
<!--  ==============================================================  -->
<!-- DICAS HOME-->
<!--  ==============================================================  --> 



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
