<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>




<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-servicos -->
  <!--  ==============================================================  -->
  <div class="container-fluid">
   <div class="row">
    <div class="container titulo-internos">
      <div class="row">
        <div class="col-xs-5 top80 text-right">
          <h1>CONHEÇA NOSSOS</h1>
          <h2>SERVIÇOS</h2>

          <!-- barra pesquisas -->
          <form action="<?php echo Util::caminho_projeto() ?>/servicos/" method="post">
            <div class="input-group stylish-input-group top10">
              <input type="text" class="form-control" name="busca_servico"  placeholder="PESQUISAR" >
              <span class="input-group-addon">
                <button type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>  
              </span>
            </div>
          </form>
          <!-- barra pesquisas -->

        </div>
      </div>
    </div> 
  </div> 
</div>
<!--  ==============================================================  -->
<!-- bg-servicos -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- SERVICOS DESCRICAO -->
<!--  ==============================================================  -->
<div class="container-fluid fundo-cinza">
  <div class="row">
    <div class="container">
      <div class="row subir-dicas bottom50">

      


      <!--  ==============================================================  -->
      <!-- SERVICOS 01 -->
      <!--  ==============================================================  -->
      <?php
      if(isset($_POST[busca_servico])):
        $complemento = "and titulo LIKE '%$_POST[busca_servico]%'";
      endif;

      $result = $obj_site->select("tb_servicos", $complemento);
       if(mysql_num_rows($result) == 0){
            echo '
                  <h6 class="top80">
                    Nenhum registro encontrado.
                  </h6>
                  ';
         }else{ 
         $i = 1;
         while ($row = mysql_fetch_array($result)) {

            if ($i % 2) { ?>
              
              <div class="col-xs-5">
                <div class="top105">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
                </div>

                <div class="top15 servicos_gerais">
                  <p><?php Util::imprime($row[descricao], 700); ?></p>
                </div>
                <div class="top10">
                  <a class="btn btn-transparente-servicos" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                    SOLICITE UM ORÇAMENTO
                  </a>
                </div>
                <div class="top20">
                  <a class="btn btn-transparente-servicos" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
                  </div>
              </div>
              <div class="col-xs-7 top50">
                <div class="servico_img">
                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 653, 467, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                    <div class="borda_servico"></div>
                  </a>
                </div>
              </div>


            <?php }else{ ?>


                <div class="col-xs-7 top50">
                  <div class="servico_img">
                    <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime(titulo); ?>">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 653, 467, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                      <div class="borda_servico"></div>
                    </a>
                  </div>
                </div>

                <div class="col-xs-5">
                  <div class="top105">
                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                  </div>

                  <div class="top15 servicos_gerais">
                    <p><?php Util::imprime($row[descricao], 700); ?></p>
                  </div>
                  <div class="top10">
                    <a class="btn btn-transparente-servicos" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                      SOLICITE UM ORÇAMENTO
                    </a>
                  </div>
                  <div class="top20">
                    <a class="btn btn-transparente-servicos" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
                    </div>
                </div>

            <?php } ?> 
            

         <?php 
          $i++;
          echo '<div class="clearfix"></div>';
         }
       }
       ?>
      <!--  ==============================================================  -->
      <!-- SERVICOS 01 -->
      <!--  ==============================================================  -->





      </div>
    </div> 
  </div>
</div>
<!--  ==============================================================  -->
<!-- SERVICOS DESCRICAO -->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
