<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-dicas  dentro-->
  <!--  ==============================================================  -->
  <div class="container-fluid">
   <div class="row">
    <div class="container">
      <div class="row titulo-internos">
        <div class="col-xs-5 top20 text-right">
          <h1>SEMPRE QUE PRECISAR</h1>
          <h2>FALE CONOSCO</h2>

          <!-- link entre paginas -->

          <div class="text-left">
            <a class="btn btn-fale active" href="#" role="button">FALE CONOSCO</a>
          </div>

          <div class="text-left top20">
            <a class="btn btn-trabalhe" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco" role="button">TRABALHE CONOSCO</a>
          </div>
          <!-- link entre paginas -->

        </div>
      </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- bg-dicas  dentro-->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- CONTATOS E FORMULARIO-->
<!--  ==============================================================  -->
<div class="container-fluid ">
  <div class="row fundo-cinza pb100">
    <div class="container">
      <div class="row top20">

        <div class="col-xs-4 contatos top60">
          <h6><span>ATENDIMENTO</span></h6>
          
          <div class="top40">
            <h5><?php Util::imprime($config[telefone1]); ?></h5>
            <p class="text-right right30"><?php Util::imprime($config[telefone1_local]); ?></p>
          </div>

          <?php if (!empty($config[telefone2])): ?>
              <div class="top40">
                <h5><?php Util::imprime($config[telefone2]); ?></h5>
                <p class="text-right right30"><?php Util::imprime($config[telefone2_local]); ?></p>
              </div>  
          <?php endif ?>

          <?php if (!empty($config[telefone3])): ?>
              <div class="top40">
                <h5><?php Util::imprime($config[telefone3]); ?></h5>
                <p class="text-right right30"><?php Util::imprime($config[telefone3_local]); ?></p>
              </div>  
          <?php endif ?>

          <?php if (!empty($config[telefone4])): ?>
              <div class="top40">
                <h5><?php Util::imprime($config[telefone4]); ?></h5>
                <p class="text-right right30"><?php Util::imprime($config[telefone4_local]); ?></p>
              </div>  
          <?php endif ?>
        </div>

        <div class="col-xs-8">
   



          <?php
          //  VERIFICO SE E PARA ENVIAR O EMAIL
          if(isset($_POST[nome]))
          {
              $texto_mensagem = "
                                Nome: ".($_POST[nome])." <br />
                                Assunto: ".($_POST[assunto])." <br />
                                Telefone: ".($_POST[telefone])." <br />
                                Email: ".($_POST[email])." <br />
                                Mensagem: <br />
                                ".(nl2br($_POST[mensagem]))."
                                ";
              Util::envia_email($config[email], utf8_decode($_POST[assunto]), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
              Util::envia_email($config[email_copia], utf8_decode($_POST[assunto]), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
              
              Util::alert_bootstrap("Obrigado por entrar em contato.");
              unset($_POST);
          }
          ?>



          <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data"> 
            <div class="fundo-formulario">
              <!-- formulario orcamento -->
              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback top5"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top5"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>   


              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top5"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                    <span class="fa fa-star form-control-feedback top5"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>  



              <div class="top15">
                <div class="col-xs-12">        
                 <div class="form-group input100 has-feedback">
                  <textarea name="mensagem" cols="30" rows="13" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                  <span class="fa fa-pencil form-control-feedback top5"></span> 
                </div>
              </div>
            </div>

            <!-- formulario orcamento -->
            <div class="col-xs-12 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn-vermelho-grande" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>

          </div>

          
          <!--  ==============================================================  -->
          <!-- FORMULARIO-->
          <!--  ==============================================================  -->
        </form>
      </div>



    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->
  <div class="container-fluid top50">
    <div class="row">
      

      <!-- Nav tabs -->
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center ul-maps">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php Util::imprime($config[src_place_local]); ?></a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php Util::imprime($config[src_place_local_1]); ?></a></li>
              </ul>          
          </div>
        </div>
      </div>
      

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <iframe src="<?php Util::imprime($config[src_place_2]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>


  

      

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->


</div>
</div>
<!--  ==============================================================  -->
<!-- CONTATOS E FORMULARIO-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>

