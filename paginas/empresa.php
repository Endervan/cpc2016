<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-empresa -->
  <!--  ==============================================================  -->
  <div class="container-fluid">
   <div class="row">
    <div class="container">
      <div class="row titulo-internos">
        <div class="col-xs-12 top120">
          <h1>CONHEÇA NOSSA</h1>
          <h2>EMPRESA</h2>
          <div class=" text-center imagem padding0">
           <img src="<?php echo Util::caminho_projeto() ?>/imgs/empresa-descricao.png" alt="">
         </div>

       </div>
     </div>
   </div> 
 </div> 
</div>
<!--  ==============================================================  -->
<!-- bg-empresa -->
<!--  ==============================================================  -->

<!--  ==============================================================  -->
<!-- empresa descricao-->
<!--  ==============================================================  -->
<div class="container-fluid fundo-cinza">
 <div class="row ">
   <div class="container">
     <div class="row empresa-descricao">
       <div class="col-xs-offset-4 col-xs-8">
         <div class="top75 scroll">
           <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
           <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>
    </div>
  </div> 
</div>
</div> 
<!--  ==============================================================  -->
<!-- empresa descricao-->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- categoria descricao-->
<!--  ==============================================================  -->
<div class="container">
 <div class="row top5 bottom70">
   <div class="col-xs-4 text-center">
    <div class="categoria-descricao">
      <a href="<?php echo Util::caminho_projeto() ?>/equipamentos" title="EQUIPAMENTOS DE ALTA QUALIDADE">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/categoria-empresa01.png" alt="">
        <h1>EQUIPAMENTOS DE ALTA QUALIDADE</h1>
      </a>
    </div>
  </div>

  <div class="col-xs-4 text-center">
    <div class="categoria-descricao">
     <a href="<?php echo Util::caminho_projeto() ?>/servicos" title="SERVIÇOS ESPECIALIZADOS">
      <img src="<?php echo Util::caminho_projeto() ?>/imgs/categoria-empresa02.png" alt="">
      <h1>SERVIÇOS ESPECIALIZADOS</h1>
    </a>
  </div>
</div>

<div class="col-xs-4 text-center">
  <div class="categoria-descricao">
    <a href="<?php echo Util::caminho_projeto() ?>/contatos" title="ATENDIMENTO ESPECIALIZADO">
      <img src="<?php echo Util::caminho_projeto() ?>/imgs/categoria-empresa03.png" alt="">
      <h1>ATENDIMENTO ESPECIALIZADO</h1>
    </a>
  </div>
</div>
</div>
</div>
<!--  ==============================================================  -->
<!-- categoria descricao-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 
<div class="container-fluid">
  <div class="row fundo-vermelho-grandiente pb80">
    <div class="container">
      <div class="row top50">

        <div class="col-xs-5 top105">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba-como-chegar-empresa.png" alt="">
        </div>

        <div class="col-xs-7">
          <div class="pull-right left10">
            <h4>
              <?php Util::imprime($config[telefone1]); ?>
              
              <?php if (!empty($config[telefone2])): ?>
                  / <?php Util::imprime($config[telefone2]); ?>
              <?php endif ?>

            </h4>
          </div>
          <div class="pull-right">
            <h3><i class="fa fa-phone-square right10"></i>ATENDIMENTO:</h3>
          </div>
          <div class="top15">
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 


<div class="clearfix"></div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
