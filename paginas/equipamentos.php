<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-dicas -->
  <!--  ==============================================================  -->
  <div class="container-fluid">
   <div class="row">
    <div class="container titulo-internos">
      <div class="row ">
        <div class="col-xs-5 top80 text-right">
          <h1>CONHEÇA NOSSOS</h1>
          <h2>EQUIPAMENTOS</h2>

          <!-- barra pesquisas -->
          <form action="<?php echo Util::caminho_projeto() ?>/equipamentos/" method="post">
            <div class="input-group stylish-input-group top20">
              <input type="text" class="form-control" name="busca_equipamentos" placeholder="PESQUISAR" >
              <span class="input-group-addon">
                <button type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>  
              </span>
            </div>
          </form>
          <!-- barra pesquisas -->

        </div>
      </div>
    </div> 
  </div> 
</div>
<!--  ==============================================================  -->
<!-- bg-dicas -->
<!--  ==============================================================  -->


<div class="container-fluid fundo-cinza pb50">
  <div class="row top70">
    <div class="container produtos-geral">
      <div class="row">



        <!--  ==============================================================  -->
        <!-- item 01-->
        <!--  ==============================================================  -->
        <?php
        if(isset($_POST[busca_equipamentos])):
          $complemento = "and titulo LIKE '%$_POST[busca_equipamentos]%'";
        endif;

        $result = $obj_site->select("tb_equipamentos", $complemento);
         if(mysql_num_rows($result) == 0){
            echo '
                  <h6 class="top80">
                    Nenhum registro encontrado.
                  </h6>
                  ';
         }else{ 
           $i = 0;
           while ($row = mysql_fetch_array($result)) {
           ?>
            <div class="col-xs-6 posicao top20 lista-equipamento">
              <div class="col-xs-8 fundo-cinza pb40">
               <div class="top30">
                <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
              </div>
              <div class=" col-xs-10">
                <p><?php Util::imprime($row[descricao], 200); ?></p>
                <span class="pull-right">...</span>  
              </div>
              <a class="btn btn-transparente-produtos top20" title="SAIBA MAIS" href="<?php echo Util::caminho_projeto() ?>/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" role="button">
                SAIBA MAIS
              </a>
            </div>
            <div class="posicao-imagem-produtos">
              <a href="<?php echo Util::caminho_projeto() ?>/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" width="280" height="207" alt="<?php Util::imprime($row[titulo]); ?>">
              </a>
            </div>
          </div>
        <?php 
          if ($i == 1) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
          
        }
      }
      ?>
      <!--  ==============================================================  -->
      <!-- item 01-->
      <!--  ==============================================================  -->

    </div>
  </div> 
 </div>
</div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
