<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#Carousel').carousel({
        interval: 5000
      })
    });
  </script>


  
</head>



<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 1 order by ordem");
                 if(mysql_num_rows($result) > 0){
                  $i = 0;
                   while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                   ?> 
                      <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                   <?php 
                    $i++;
                   }
                }
                ?>
              </ol>

              <!-- Controls -->
              <a class="left carousel-control setas-home" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control setas-home" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>
          </div>
        </div> 
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  --> 
        

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          

          <?php 
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
            ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                <div class="carousel-caption">

                    <div class="container">
                      <div class="row">
                        <div class="col-xs-12 text-left">
                          <h5><?php Util::imprime($imagem[titulo]); ?></h5>
                          <div class="col-xs-5 top20 padding0">
                            <p><?php Util::imprime($imagem[legenda]); ?></p>
                            
                            <?php if (!empty($imagem[url])): ?>
                            <div class="top20">
                              <a class="btn btn-transparente" href="<?php Util::imprime($imagem[url]); ?>" role="button">
                                SAIBA MAIS
                              </a>
                            </div>
                            <?php endif; ?>

                          </div>
                        </div>
                      </div>
                    </div>

                </div>
              </div>
            <?php
              $i++;
            }
          }
          ?>


        </div>


      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->



 <!--  ==============================================================  -->
  <!-- PRODUTOS HOME-->
  <!--  ==============================================================  -->
  <div class="container ">
    <div class="row produtos-geral bottom80">
      <div class="col-xs-12">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/descricao-produtos.png" alt="">
      </div>
      <div id="Carousel" class="carousel slide">

      

        <!-- Carousel items -->
        <div class="carousel-inner">




          <div class="item active">
            

            <?php
            $result = $obj_site->select("tb_equipamentos", "order by rand() limit 4");
             if(mysql_num_rows($result) > 0){
              $i = 0;
               while ($row = mysql_fetch_array($result)) {
               ?> 
                  <div class="col-xs-6 posicao top20 lista-equipamento">
                    <div class="col-xs-8 fundo-cinza pb40">
                     <div class="top30">
                      <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
                    </div>
                    <div class=" col-xs-10">
                      <p><?php Util::imprime($row[descricao], 200); ?></p>
                      <span class="pull-right">...</span>  
                    </div>
                    <a class="btn btn-transparente-produtos top20" title="SAIBA MAIS" href="<?php echo Util::caminho_projeto() ?>/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" role="button">
                      SAIBA MAIS
                    </a>
                  </div>
                  <div class="posicao-imagem-produtos">
                    <a href="<?php echo Util::caminho_projeto() ?>/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" width="280" height="207" alt="<?php Util::imprime($row[titulo]); ?>">
                    </a>
                  </div>
                </div>

               <?php 

                  if ($i == 1) {
                    echo '<div class="clearfix"></div>';
                    $i = 0;
                  }else{
                    $i++;
                  }


               }
             }
             ?>

      </div><!--.item-->



      </div><!--.carousel-inner-->
      
      </div><!--.Carousel-->
  </div>
</div><!--.container-->
<!--  ==============================================================  -->
<!-- PRODUTOS HOME-->
<!--  ==============================================================  --> 








<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 
<div class="container-fluid">
  <div class="row fundo-vermelho-grandiente pb80">
    <div class="container">
      <div class="row top50">

        <div class="col-xs-5">
          <div class="text-center top10">
            <img src=" <?php echo Util::caminho_projeto() ?>/imgs/descricao-conheca.png" alt="">
          </div>
          <div class="top20"> 
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
           <div class="top15">
              <a class="btn btn-transparente-conheca top15" href="<?php echo Util::caminho_projeto() ?>/empresa" role="button">SAIBA MAIS</a>
                <div class="pull-right top15">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba-como-chegar-home.png" alt="">
                </div>
           </div>
           <div class="top15">
              <a class="btn btn-transparente-conheca top15" href="<?php echo Util::caminho_projeto() ?>/contatos" role="button">FALE CONOSCO</a>
           </div>
        </div>

        <div class="col-xs-7">
          <div class="pull-right left10">
              <h4>
                <?php Util::imprime($config[telefone1]); ?>
                <?php if (!empty($config[telefone2])): ?>
                  / <?php Util::imprime($config[telefone2]); ?>
                <?php endif ?>
              </h4>
            </div>
            <div class="pull-right">
              <h3><i class="fa fa-phone-square right10"></i>ATENDIMENTO:</h3>
            </div>
            <div class="top15">
              <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 



<!--  ==============================================================  -->
<!-- SERVICOS HOME-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row servicos-home">
    <div class="col-xs-6">
      <img src="<?php echo Util::caminho_projeto() ?>/imgs/servicos-home.png" alt="">
    </div>

    <div class="col-xs-6 top40">
      
      <?php
      $result = $obj_site->select("tb_servicos", "order by rand() limit 4");
       if(mysql_num_rows($result) > 0){
        $i = 0;
         while ($row = mysql_fetch_array($result)) {
         ?> 
            <div class="media top30">
              
                <div class="media-left media-middle">
                    <i class="fa fa-star" aria-hidden="true" class="media-object"></i>
                </div>
                <div class="media-body">
                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <p class="media-heading"><?php Util::imprime($row[titulo]); ?></p>
                  </a>
                </div>
              
            </div>
         <?php 
         }  
       }
       ?>


     
    </div>

  </div>
</div> 
<!--  ==============================================================  -->
<!-- SERVICOS HOME-->
<!--  ==============================================================  --> 


<!--  ==============================================================  -->
<!-- DICAS HOME-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row top10">


    <?php
    $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
     if(mysql_num_rows($result) > 0){
      $i = 0;
       while ($row = mysql_fetch_array($result)) {
       ?> 
        <!-- item 01 -->
              <div class="col-xs-4 text-center">
                <div class="thumbnail dicas-home">
                  <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 358, 327, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                    <div class="borda-interna"></div>
                  </a>
                  <div class="caption">
                    <div class="top20">
                      <p><?php Util::imprime($row[titulo]); ?></p>
                    </div>
                    <a class="btn btn-transparente-conheca top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>">
                      SAIBA MAIS
                    </a>
                  </div>
                </div>
              </div>
       <?php 
         }  
       }
       ?>

   
    

    <div class="clearfix"></div>


  </div>
</div> 
<!--  ==============================================================  -->
<!-- DICAS HOME-->
<!--  ==============================================================  --> 


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
