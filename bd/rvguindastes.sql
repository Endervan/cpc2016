-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 16-Out-2019 às 19:03
-- Versão do servidor: 5.5.60-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `rvguindastes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'LOCAÇÃO DE GUINDASTES', '1807201803498142271145..jpg', 'SIM', 0, '2', 'locacao-de-guindastes', 'equipamento/locacao-de-guindastes', 'RV Guindastes e Transportes conta com guindastes novos e preparados para atividades que exigem muita técnica e manutenção preventiva.', NULL),
(2, 'LOCAÇÃO SERVIÇO MUNCK', '1807201803527794720253..jpg', 'SIM', 0, '2', 'locacao-servico-munck', '/mobile/produtos', 'Caminhões trucados ou toco equipados com guindaste munck. A RV Guindastes e Transportes também possui munck equipado com guincho de cabo.', NULL),
(3, 'LOCAÇÃO DE GUINDASTES', '1807201803553994095163..jpg', 'SIM', 0, '1', 'locacao-de-guindastes', '/equipamento/locacao-de-guindastes', 'A RV Guindastes e Transportes conta com guindastes novos e preparados para atividades que exigem muita técnica e manutenção preventiva, com operadores treinados e capacitados pelo fabricante e ou representante da industria.', NULL),
(5, 'LOCAÇÃO SERVIÇO MUNCK', '1807201803587567008137..jpg', 'SIM', 0, '1', 'locacao-servico-munck', '/equipamento/locacao-de-caminhoes-munck', 'Caminhões trucados ou toco equipados com guindaste munck. A RV Guindastes e Transportes também possui munck equipado com guincho de cabo.', NULL),
(6, 'IÇAMENTOS', '1807201804016566003271..jpg', 'SIM', 0, '1', 'icamentos', '/servico/icamentos', 'Com a RV Guindastes e Transportes você ou sua empresa pode garantir içamentos de produtos e equipamentos em prédios comerciais e residenciais.', NULL),
(7, 'IÇAMENTOS', '1807201804058515188665..jpg', 'SIM', 0, '2', 'icamentos', '/MOBILE', 'Com a RV Guindastes você ou sua empresa pode garantir içamentos de produtos e equipamentos em prédios comerciais e residenciais.', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE IF NOT EXISTS `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idbannerinterna`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '0205201608361385872590.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Serviços', '0205201611431252330781.jpg', NULL, 'servicos', 'SIM', NULL),
(3, 'Equipamentos', '0205201608361385872590.jpg', NULL, NULL, 'SIM', NULL),
(4, 'Equipamentos dentro', '0205201608361385872590.jpg', NULL, NULL, 'SIM', NULL),
(5, 'Dicas', '0705201601061209689027.jpg', NULL, 'dicas', 'SIM', NULL),
(6, 'Mobile - Fale conosco', '0405201608371223164232.jpg', NULL, 'mobile--fale-conosco', 'SIM', NULL),
(7, 'Dicas Internas', '0705201601161340189585.jpg', NULL, 'dicas-internas', 'SIM', NULL),
(8, 'Contato', '1905201604385059843406.jpg', NULL, 'contato', 'SIM', NULL),
(9, 'Trabalhe Conosco', '0705201601171211805647.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(10, 'Mobile- orcamento', '0505201611471338738970.jpg', NULL, 'mobile-orcamento', 'SIM', NULL),
(11, 'Mobile-produtos', '0505201603031217334404.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(12, 'Mobile-servicos', '0505201608531296497808.jpg', NULL, 'mobileservicos', 'SIM', NULL),
(13, 'Mobile-dicas', '0705201601281295129304.jpg', NULL, 'mobiledicas', 'SIM', NULL),
(14, 'Mobile-equipamentos', NULL, NULL, NULL, 'SIM', NULL),
(15, 'Orçamento', '0705201609461136659878.jpg', NULL, 'orcamento', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoriaproduto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=83 ;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(75, 'AQUECEDOR SOLAR PARA PISCINAS', NULL, '1804201603221326960860.png', 'SIM', NULL, 'aquecedor-solar-para-piscinas', NULL, NULL, NULL),
(74, 'AQUECEDOR SOLAR RESIDENCIAL', NULL, '1804201603211272181447.png', 'SIM', NULL, 'aquecedor-solar-residencial', NULL, NULL, NULL),
(76, 'SISTEMAS SOLAR PARA RESITÊNCIA', NULL, '1804201603221235154182.png', 'SIM', NULL, 'sistemas-solar-para-resitencia', NULL, NULL, NULL),
(77, 'CONSTRUÇÃO DE PISCINAS', NULL, '1804201603231118321922.png', 'SIM', NULL, 'construcao-de-piscinas', NULL, NULL, NULL),
(78, 'PRODUTOS PARA PISCINAS', NULL, '1804201603231279479520.png', 'SIM', NULL, 'produtos-para-piscinas', NULL, NULL, NULL),
(79, 'TRATAMENTO PARA PISCINAS', NULL, '1804201603231356950292.png', 'SIM', NULL, 'tratamento-para-piscinas', NULL, NULL, NULL),
(80, 'AQUECEDOR SOLAR PARA PISCINAS', NULL, '1804201603241252158870.png', 'SIM', NULL, 'aquecedor-solar-para-piscinas', NULL, NULL, NULL),
(81, 'PRODUTOS PARA PSICINAS', NULL, '1804201603291290410810.png', 'SIM', NULL, 'produtos-para-psicinas', NULL, NULL, NULL),
(82, 'CASCATAS', 81, '2304201608131207064762.png', 'SIM', NULL, 'cascatas', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) DEFAULT 'NAO',
  `telefone1_local` varchar(255) NOT NULL,
  `telefone2_local` varchar(255) NOT NULL,
  `telefone3_local` varchar(255) NOT NULL,
  `telefone4_local` varchar(255) NOT NULL,
  `endereco_2` varchar(255) NOT NULL,
  `src_place_2` varchar(255) NOT NULL,
  `src_place_local` varchar(255) NOT NULL,
  `src_place_local_1` varchar(255) NOT NULL,
  PRIMARY KEY (`idconfiguracao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `super_admin`, `telefone1_local`, `telefone2_local`, `telefone3_local`, `telefone4_local`, `endereco_2`, `src_place_2`, `src_place_local`, `src_place_local_1`) VALUES
(1, '', '', '', 'SIM', 0, '', 'SIN CJ. A CONJ. A Parte 4 SIA CEP: 71225-000 / Brasília - DF', '(61) 3465-2538', '(61) 99271-5507', 'cpctransportes@cpctransportes.com.br', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15360.351720029677!2d-47.924734!3d-15.746487!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8d8205778267d3f1!2sCPC+Bras%C3%ADlia+Aluguel+de+Guindastes!5e0!3m2!1spt-BR!2sbr!4v1463413308856', NULL, NULL, 'junior@homewebbrasil.com.br, suporte@masmidia.com.br, angela.homeweb@gmail.com, compras@cpctransportes.com.br, mabonatofontes@gmail.com', 'https://plus.google.com/112052068835680720553', '(61) 98364-0009', '', 'NAO', '', '', '', '', '', '', 'Brasília', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`iddica`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=42 ;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'BENEFÍCIOS DA UTILIZAÇÃO DE GUINDASTES', '<p style="text-align: justify;">\r\n	Uma grua m&oacute;vel pode ser uma m&aacute;quina muito simples, com apenas uma lan&ccedil;a telesc&oacute;pica em sua plataforma, ou um guindaste de tamanho completo equipado com diversos acess&oacute;rios. Os guindastes standard s&atilde;o m&aacute;quinas usadas para levantar objetos de tamanhos grandes e pesados. Eles s&atilde;o equipados com correntes, cabos de a&ccedil;o, tambores e pain&eacute;is de controle. Porque diferentes aplica&ccedil;&otilde;es requerem diferentes tipos de guindastes, o mercado oferece v&aacute;rios guindastes, de pequeno e caminh&atilde;o montados lan&ccedil;a, matou, pequeno, grande e m&oacute;vel guindaste hidr&aacute;ulico,.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Por exemplo, existem muitas opera&ccedil;&otilde;es de eleva&ccedil;&atilde;o em que, utilizando um guindaste est&aacute;tico n&atilde;o seria pr&aacute;tico. Existe uma vasta gama de situa&ccedil;&otilde;es urbanas onde o tra&ccedil;ado das ruas e edif&iacute;cios impede um guindaste est&aacute;tico de ser configurado para elevadores especiais, tais como levantamento est&aacute;tua ou elevadores interiores. Em tais casos, um guindaste m&oacute;vel &eacute; a melhor solu&ccedil;&atilde;o poss&iacute;vel. A maior vantagem do guindaste m&oacute;vel &eacute; a sua flexibilidade e capacidade de acesso a &aacute;reas e locais menores com espa&ccedil;o limitado para a entrada. Mas, o guindaste m&oacute;vel oferece mais do que apenas uma entrada f&aacute;cil. Leia mais para descobrir quais s&atilde;o os benef&iacute;cios mais importantes fornecidos pelo guindaste m&oacute;vel.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o exige muito espa&ccedil;o - Normalmente, os locais de constru&ccedil;&atilde;o s&atilde;o grandes o suficiente para o equipamento pesado que est&aacute; sendo usado para mover-se facilmente. Mas, quando a constru&ccedil;&atilde;o tem lugar em &aacute;reas urbanas, o local de trabalho &eacute; pequeno e apertado. Por exemplo, em tais &aacute;reas, usando o guindaste de torre para a constru&ccedil;&atilde;o de edif&iacute;cios altos, &eacute; quase imposs&iacute;vel, porque este tipo guindaste exige espa&ccedil;o. O guindaste m&oacute;vel, por outro lado, precisa de apenas uma parte da &aacute;rea de trabalho para realizar com &ecirc;xito as tarefas de levantamento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Resistente o suficiente para lidar com v&aacute;rias tarefas de levantamento - Embora o guindaste m&oacute;vel &eacute; significativamente menor quando comparado a outros tipos de guindaste, que &eacute; capaz e poderoso o suficiente para levantar objetos pesados, como o guindaste de torre. Com seus m&uacute;ltiplos eixos e energia hidr&aacute;ulica avan&ccedil;ada, o guindaste m&oacute;vel ainda pode levantar objetos pesados ??e materiais para grandes alturas. Portanto, com a grua m&oacute;vel, o poder n&atilde;o &eacute; um problema quando se trata de levantar e mover objetos no local da constru&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Voc&ecirc; sabe o que &eacute; necess&aacute;rio para a constru&ccedil;&atilde;o de um guindaste de torre? Definitivamente mais do que aquilo que &eacute; necess&aacute;rio para instalar um guindaste m&oacute;vel. O tempo necess&aacute;rio para a instala&ccedil;&atilde;o de uma grua m&oacute;vel em um canteiro de obras &eacute; muito curto. Estes tipos de guindastes precisa apenas de um pouco de espa&ccedil;o a ser estabilizado. Isto significa que o trabalho pode ser completado muito mais r&aacute;pido.</p>', '1705201606032410502243..jpg', 'SIM', NULL, 'beneficios-da-utilizacao-de-guindastes', 'A RV Locação de Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília - DF', '', '', NULL),
(37, 'CAMINHÕES MUNCK - DICAS DE SEGURANÇA', '<p style="text-align: justify;">\r\n	Voc&ecirc;s devem saber que o Munck &eacute; um guindaste comandado hidraulicamente e instalado sobre o chassi de um caminh&atilde;o, com grande utiliza&ccedil;&atilde;o na movimenta&ccedil;&atilde;o, remo&ccedil;&atilde;o, levantamento e transporte de cargas relativamente leve.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Somente pessoas habilitadas com Carteira Nacional de Habilita&ccedil;&atilde;o, categorias C, D ou E, podem conduzir o caminh&atilde;o Munck.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Antes do inicio da atividade, verifique se as pessoas s&atilde;o treinadas e autorizadas para operar o caminh&atilde;o Munck.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A utiliza&ccedil;&atilde;o do caminh&atilde;o Munck &eacute; limitada de acordo com sua capacidade e com o tipo de carga que ser&aacute; movimentada, cabe ao T&eacute;cnico em Seguran&ccedil;a, supervisor ou operador do Munck analisarem a viabilidade de sua utiliza&ccedil;&atilde;o antes de iniciar o servi&ccedil;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A &aacute;rea do piso onde ser&atilde;o apoiadas as sapatas deve ser plana e firme e s&oacute; colocar cal&ccedil;os resistentes sob as sapatas se houver necessidade de pequena corre&ccedil;&atilde;o do n&iacute;vel do caminh&atilde;o, verificar se os freios est&atilde;o acionados e com as rodas cal&ccedil;adas.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A &aacute;rea coberta pelo raio de a&ccedil;&atilde;o da lan&ccedil;a e da carga deve ser isolada, sinalizada com cones e livre de quaisquer obst&aacute;culos e n&atilde;o permitir o tr&acirc;nsito de pessoas no local.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para casos de opera&ccedil;&atilde;o perto de redes el&eacute;tricas ou equipamentos energizados e, ou valas e escava&ccedil;&otilde;es s&atilde;o necess&aacute;rias redobrar a aten&ccedil;&atilde;o de todos envolvidos na opera&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Antes de se iniciar qualquer opera&ccedil;&atilde;o de carga ou descarga em valas e escava&ccedil;&otilde;es, o pessoal que estiver trabalhando naqueles locais deve ser removido, e s&oacute; deve retornar ao t&eacute;rmino da opera&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Utilize somente eslingas de cabo de a&ccedil;o, cintas ou outro dispositivo espec&iacute;fico para i&ccedil;amento da carga. Nunca utilize cordas para o i&ccedil;amento de cargas, elas dever&atilde;o ser utilizadas apenas para posicionar/direcionar a carga.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&Eacute; obrigat&oacute;ria a exist&ecirc;ncia da trava de seguran&ccedil;a no gancho.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nunca permanecer sobre a carro&ccedil;aria na &aacute;rea de alcance da lan&ccedil;a enquanto a mesma estiver em movimento ou permane&ccedil;a sob a cargas suspensas.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O operador nunca deve abandonar o Munck com a carga suspensa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o arrastar cargas, porque o guincho do Munck n&atilde;o foi projetado para tracionar, e sim para efetuar levantamento vertical.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nunca movimentar o ve&iacute;culo com a carga suspensa, pois a estabilidade do mesmo ficar&aacute; seriamente reduzida, gerando risco de queda da carga sobre pessoas ou equipamentos.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A movimenta&ccedil;&atilde;o do caminh&atilde;o Munck de uma &aacute;rea para outra deve ser feita com as patolas e lan&ccedil;as recolhidas e posicionadas em seu ber&ccedil;o de apoio.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O operador dever&aacute; posicionar-se em local mais afastado poss&iacute;vel da &aacute;rea de atua&ccedil;&atilde;o da lan&ccedil;a, preservando sua seguran&ccedil;a e de frente para a movimenta&ccedil;&atilde;o do equipamento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O caminh&atilde;o Munck dever&aacute; ser equipado com comandos duplos em ambos os lados do ve&iacute;culo e, com uma tabela de carga impressa fixada no caminh&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	Inspe&ccedil;&otilde;es peri&oacute;dicas devem ser realizadas nos ganchos, a fim de detectar poss&iacute;veis deformidades.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Inspecionar o estado dos cabos, cintas ou quaisquer outros dispositivos que ser&atilde;o usados antes do inicio do i&ccedil;amento da carga.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fonte:&nbsp;http://conselhoeseguranca.blogspot.com.br/2012/03/caminhao-munck-dds.html</p>', '1905201602257578401424..jpg', 'SIM', NULL, 'caminhoes-munck--dicas-de-seguranca', 'RV Locações de Transportes - Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília - DF', '', '', NULL),
(38, 'PRINCIPAIS VANTAGENS DE GUINDAUTOS', '<p style="text-align: justify;">\r\n	A seguir est&atilde;o as principais vantagens da utiliza&ccedil;&atilde;o de servi&ccedil;os de caminh&atilde;o guindaste para seus locais de constru&ccedil;&atilde;o:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	1. Eleva&ccedil;&atilde;o segura: A tarefa mais dif&iacute;cil em locais &eacute; o movimento de equipamento pesado, porque &agrave;s vezes pode ser perigoso para a vida dos trabalhadores. &Agrave;s vezes, cabos e correntes tamb&eacute;m ficar agarrado com muito peso e que torna a tarefa mais dif&iacute;cil. Mas este problema pode ser facilmente resolvido com gruas auxiliares que t&ecirc;m capacidade de levantar objetos muito pesados de um lugar para outro; ele tamb&eacute;m faz diminuir o risco de acidente no ambiente de trabalho.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	2. Menos tempo de inatividade: Levantar equipamentos pesados usando o poder do homem tamb&eacute;m &eacute; uma tarefa demorada e tamb&eacute;m aumenta o custo para o trabalho. Guindautos podem fazer sua tarefa simples e oferecem op&ccedil;&otilde;es em movimento r&aacute;pido, mesmo para locais distantes.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	3. Acessibilidade: Uma dos benef&iacute;cios das gruas m&oacute;veis &eacute; que voc&ecirc; pode facilmente us&aacute;-los em locais remotos pois funcionam utlizando energia do pr&oacute;prio guindauto: electricidade, g&aacute;s, bateria ou combust&iacute;vel.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Todas essas vantagens provam que guindautos completam os seus trabalhos dif&iacute;ceis em canteiros de obras sem qualquer problema. Se voc&ecirc; est&aacute; procurando esse tipo de equipamento para o seu local de trabalho, entre em contato com prestadores de servi&ccedil;os especializados, os quais &nbsp;fornecem um operador de guindaste treinados para a tarefa.</p>', '1905201602247462330100..jpg', 'SIM', NULL, 'principais-vantagens-de-guindautos', 'RV Locações de Transportes - Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília - DF', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Index - Empresa', '<div style="text-align: justify;">\r\n	Com grande experi&ecirc;ncia no mercado de transporte de m&aacute;quinas operatrizes, geradores, containers, caldeiras, tanques, empilhadeiras, m&aacute;quinas gr&aacute;ficas, etc e com os mais modernos m&eacute;todos de remo&ccedil;&otilde;es e i&ccedil;amentos (inclusive pain&eacute;is). A RV Guindastes e Transportes oferece qualidade, bom atendimento e custos justos com confiabilidade e honestidade fazem parte das obriga&ccedil;&otilde;es da empresa.</div>', 'SIM', 0, '', '', '', 'index--empresa', NULL, NULL, NULL),
(2, 'Empresa', '<p style="text-align: justify;">\r\n	A RV Guindastes &eacute; uma empresa de Transportes Pesados que atua com excel&ecirc;ncia na presta&ccedil;&atilde;o de servi&ccedil;os de remo&ccedil;&atilde;o t&eacute;cnica de m&aacute;quinas, loca&ccedil;&atilde;o de equipamentos e montagem de estruturas met&aacute;licas. Tem como miss&atilde;o, proporcionar servi&ccedil;os de alta qualidade em movimenta&ccedil;&atilde;o e transportes de cargas, que atendam as necessidades espec&iacute;ficas de seus clientes.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Com grande experi&ecirc;ncia no mercado de transporte de m&aacute;quinas operatrizes, geradores, containers, caldeiras, tanques, empilhadeiras, m&aacute;quinas gr&aacute;ficas, etc e com os mais modernos m&eacute;todos de remo&ccedil;&otilde;es e i&ccedil;amentos (inclusive pain&eacute;is).</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A RV Guindastes Transportes oferece qualidade, bom atendimento e custos justos com confiabilidade e pontualidade fazem parte das obriga&ccedil;&otilde;es da empresa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Venha para a RV Guindastes e Transportes, aqui nosso cliente tem a garantia do servi&ccedil;o realizado com quem entende de peso pesado.</p>', 'SIM', 0, '', '', '', 'empresa', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipamentos`
--

CREATE TABLE IF NOT EXISTS `tb_equipamentos` (
  `idequipamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `imagem_interna` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idequipamento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `tb_equipamentos`
--

INSERT INTO `tb_equipamentos` (`idequipamento`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`, `imagem_interna`) VALUES
(1, 'LOCAÇÃO DE GUINDASTES', '1705201601406597381743.png', '<p style="text-align: justify;">\r\n	Atuando com equipamentos vindos de pa&iacute;ses em pleno crescimento social e econ&ocirc;mico, a RV Guindastes e Transportes conta com guindastes novos e preparados para atividades que exigem muita t&eacute;cnica e manuten&ccedil;&atilde;o preventiva. Ainda, disp&otilde;em de operadores treinados e capacitados pelo fabricante e ou representante da industria.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Capacidade de 30 a 100 toneladas para a execu&ccedil;&atilde;o dos mais variados servi&ccedil;os:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Remo&ccedil;&atilde;o de m&aacute;quinas;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Montagem de pontes rolantes;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Montagem e manuten&ccedil;&atilde;o de prensas;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Carga e descarga de m&aacute;quinas em geral</p>\r\n<p style="text-align: justify;">\r\n	- Premontagens em geral(vigas, perfis, postes, antenas, etc)</p>', 'RV Guindastes e Transportes Locações e Aluguel Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília - DF', 'Locação de Guindastes DF, Aluguel de Guindastes DF, Aluguel de Caminhões Munk DF, Aluguel de Munck DF, Locação de Caminhões Munk DF, Locação de Munck DF, Locação de Guindastes Rio Verde Goiás, Aluguel de Guindastes Rio Verde Goiás, Aluguel de Caminhões Rio Verde Goiás, Aluguel de Rio Verde Goiás, Locação de Caminhões Rio Verde Goiás, Locação de Rio Verde Goiás.', 'RV Guindastes e Transportes: Locações e Aluguel Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília - DF', 'SIM', 0, 'locacao-de-guindastes', 80, '3M', NULL, NULL, '', '', 1, '1807201804332137070422..jpg'),
(2, 'LOCAÇÃO DE CARRETAS', '1605201605576400341399.png', '<p style="text-align: justify;">\r\n	Ideal para o transporte de carga utilizando tr&ecirc;s eixos, &eacute; preparado para viagens de entrega de pequena, m&eacute;dia e longas dist&acirc;ncias, tamb&eacute;m &eacute;, uma excelente alternativa para transportes de equipamentos extra-pesado.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Carga &uacute;til mais carroceria: 45 toneladas</p>', 'RV Guindastes e Transportes Locações de Carretas DF, Locação de Carretas Rio Verde Goiás.', '', 'RV Guindastes e Transportes trabalha com Locações e Aluguel de Carretas em Brasília no DF e Locação de Carretas em Rio Verde Goiás.', 'SIM', 0, 'locacao-de-carretas', 77, 'Tigre', NULL, NULL, '', '', 3, '0405201610431245857231..jpg'),
(7, 'LOCAÇÃO DE CARRETAS EXTENSIVAS', '1605201606045511159391.png', '<p style="text-align: justify;">\r\n	Carreta tipo prancha, articulada que aberto chega a 24 metros de cumprimento. A Carreta extensiva serve para transporte de cargas especiais com tamanho grande. ideal para transportar vag&otilde;es de trem, encanamentos gigantes, pe&ccedil;as pre moldadas da constru&ccedil;&atilde;o civil, plataformas e muito mais...&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Capacidade de at&eacute; 50 toneladas para a execu&ccedil;&atilde;o dos mais variados servi&ccedil;os:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Remo&ccedil;&atilde;o de m&aacute;quinas;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Montagem de pontes rolantes;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Montagem e manuten&ccedil;&atilde;o de prensas;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Carga e descarga de m&aacute;quinas em geral</p>\r\n<p style="text-align: justify;">\r\n	- Premontagens em geral(vigas, perfis, postes, antenas, etc).</p>', 'RV Guindastes e Transportes Locações de Carretas Extensivas Tipo Prancha DF, Locação de Carretas Extensivas Tipo Prancha Rio Verde Goiás.', '', 'RV Guindastes e Transportes trabalha com Locações e Aluguel de Carretas Extensivas Tipo Prancha em Brasília no DF e Locação de Carretas Extensivas Tipo Prancha em Rio Verde Goiás.', 'SIM', 0, 'locacao-de-carretas-extensivas', 82, 'Amanco', NULL, NULL, '', '', 4, '1705201602262399313106..jpg'),
(8, 'LOCAÇÃO DE CAMINHÕES MUNCK', '1705201601416131354930.png', '<p style="text-align: justify;">\r\n	Caminh&otilde;es trucados ou toco equipados com guindaste (munck) com capacidade de 4 a 14 toneladas, com lan&ccedil;as variando entre 16 e 29 metros de alcance na posi&ccedil;&atilde;o vertical. A Empresa tamb&eacute;m possui munck equipado com guincho de cabo.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Atendimento com diversas capacidades;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	- Para a eleva&ccedil;&atilde;o de pequenos pesos at&eacute; 14 toneladas;</p>\r\n<p style="text-align: justify;">\r\n	- Lan&ccedil;a telesc&oacute;pias que atingem dist&acirc;ncia de at&eacute; 29m.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	<strong>Ideal para:</strong></p>\r\n<p style="text-align: justify;">\r\n	- Montagem de estruturas met&aacute;licas.</p>\r\n<p style="text-align: justify;">\r\n	- Montagem de estruturas pr&eacute;-moldadas de concreto.</p>\r\n<p style="text-align: justify;">\r\n	- Instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o de cobertura de galp&otilde;es.</p>\r\n<p style="text-align: justify;">\r\n	- Instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o de Luminosos.</p>\r\n<p style="text-align: justify;">\r\n	- I&ccedil;amento de piscinas, sof&aacute;s, quadros, televisores, cofre, etc.</p>', 'RV Guindastes e Transportes Locações de Caminhões Munck DF, Locação de Caminhões Munck Rio Verde Goiás.', '', 'RV Guindastes e Transportes oferece aluguel e locação de Guindastes e Caminhões Munck , Equipamentos para Içamentos e Muncks em Brasília DF e aluguel e locação de Guindastes e Caminhões Munck Rio Verde Goiás.', 'SIM', 0, 'locacao-de-caminhoes-munck', 80, 'Sol', NULL, NULL, '', '', 1, '1805201601019519990517..jpg'),
(9, 'LOCAÇÃO DE CARRETA PRANCHA', '1807201804411713307715.jpg', '<div style="text-align: justify;">\r\n	Com prancha fixa para at&eacute; 3 ve&iacute;culos e com 10 metros, redutor e chassis alongado, a frota de caminh&otilde;es prancha da CPC est&atilde;o dispon&iacute;veis para as mais diversas situa&ccedil;&otilde;es, dando destaque ao transporte de ve&iacute;culos de pequeno e m&eacute;dio porte em com problemas oriundos de situa&ccedil;&otilde;es adversas.</div>', 'RV Guindastes e Transportes Locações de Carretas Tipo Prancha DF, Locação de Carretas Tipo Prancha Rio Verde Goiás.', '', 'RV Guindastes e Transportes Transportes trabalha com Locações e Aluguel de Carretas Extensivas Tipo Prancha em Brasília no DF e Locação de Carretas Extensivas Tipo Prancha em Rio Verde Goiás.', 'SIM', 0, 'locacao-de-carreta-prancha', 81, NULL, NULL, NULL, '', '', NULL, '1905201605072710760844..jpg'),
(10, 'LOCAÇÃO DE EMPILHADEIRAS', '0405201601491120218923.png', '<div style="text-align: justify;">\r\n	Empilhadeiras s&atilde;o utilizadas para grande movimenta&ccedil;&atilde;o de cargas. &nbsp;Garantindo agilidade, redu&ccedil;&atilde;o de tempo, economia de m&atilde;o-de-obra e compacta&ccedil;&atilde;o de espa&ccedil;o. &nbsp;&nbsp;</div>', 'RV Guindastes e Transportes Locação de Empilhadeiras DF, Locação de Empilhadeiras Rio Verde Goiás.', '', 'RV Guindastes e Transportes trabalha com Locação e Aluguel de Empilhadeiras em Brasília no DF e  Locação e Aluguel de Empilhadeiras em Rio Verde Goiás.', 'SIM', 0, 'locacao-de-empilhadeiras', 77, NULL, NULL, NULL, '', '', NULL, '1805201606386812143483..jpg'),
(11, 'LOCAÇÃO DE TIFÔS MACACO HIDRÁULICO', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'CPC Locações de Tifôs Macaco Hidráulico', '', '', 'NAO', 0, 'locacao-de-tifos-macaco-hidraulico', 77, NULL, NULL, NULL, 'https://www.youtube.com/embed/xIM22tZAdwE', '<p>\r\n	Apresenta&ccedil;&atilde;o do v&iacute;deo Lorem Ipsum is simply dummy text of the printing and typesettin g industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a gall ey of type and scrambled it to make a type specimen book. It h as survived not only five centuries, but alsothe leap into electro nic typesetting, remaining essentially unchanged. It was popularis ed in the 1960</p>', NULL, '0405201610431245857231..jpg'),
(12, 'LOCAÇÃO DE PALETEIRAS', '1605201606083962410452.png', '<p style="text-align: justify;">\r\n	Paleteiras manuais s&atilde;o equipamentos utilizados no processo de movimenta&ccedil;&atilde;o de cargas paletizadas.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Apresenta-se tamb&eacute;m com o nome de paleteiras hidr&aacute;ulicas, transpaletes ou carros hidr&aacute;ulicos, e na log&iacute;stica &eacute; considerado o equipamento b&aacute;sico e est&aacute; presente em todas as empresas que precisam transportar ou armazenar cargas em paletes.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A paleteira Paletrans &eacute; um equipamento de opera&ccedil;&atilde;o f&aacute;cil e de manuten&ccedil;&atilde;o simples.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Basta um treinamento b&aacute;sico para o operador manuse&aacute;-la.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Existem diversos tipos de paleteiras, espec&iacute;ficas para cada necessidade.</p>', 'RV Guindastes e TransportesLocação de Paleteiras DF, Locação de Paleteiras Rio Verde Goiás.', '', 'RV Guindastes e Transportes trabalha com Locações e Aluguel de Paleteiras em Brasília no DF e Locação de Paleteiras em Rio Verde Goiás.', 'SIM', 0, 'locacao-de-paleteiras', 75, NULL, NULL, NULL, '', '', NULL, '1805201606569750820605..jpg'),
(13, 'LOCAÇÃO DE CONTAINERS', '1605201606227642199562.png', '<p style="text-align: justify;">\r\n	Os containers locados na RV Guindastes e Transportes&nbsp;s&atilde;o utilizados para os mais diversos fins, tais como:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Canteiros de obras</p>\r\n<p style="text-align: justify;">\r\n	Almoxarifados</p>\r\n<p style="text-align: justify;">\r\n	Sanit&aacute;rios</p>\r\n<p style="text-align: justify;">\r\n	Refeit&oacute;rios</p>\r\n<p style="text-align: justify;">\r\n	Vesti&aacute;rios</p>\r\n<p style="text-align: justify;">\r\n	Dormit&oacute;rios</p>\r\n<p style="text-align: justify;">\r\n	Etc.</p>', 'RV Guindastes e Transportes Locações de Container Marítimos DF, Locação Locações de Container Marítimos Rio Verde Goiás.', '', 'RV Guindastes e Transportes trabalha com Locações e Aluguel de Container Marítimos em Brasília no DF e Locação de Container Marítimos em Rio Verde Goiás.', 'SIM', 0, 'locacao-de-containers', 74, NULL, NULL, NULL, '', '', NULL, '1905201603475502612499..jpg'),
(14, 'LOCAÇÃO DE BOX - SELF STORAGE', '0806201711388435496236.jpg', '<div>\r\n	A&nbsp;<span style="text-align: justify;">RV</span>&nbsp;Storage e Guarda M&oacute;veis DF, &nbsp;possui o espa&ccedil;o ideal para pessoas e para empresas.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Seja para guardar m&oacute;veis, mudan&ccedil;as, volumes, arquivos, documentos, estoque ou o que voc&ecirc; precisar. Boxes exclusivos, de acordo com suas necessidades.</div>\r\n<div>\r\n	A&nbsp;<span style="text-align: justify;">RV&nbsp;</span>Self Storage voc&ecirc; pode armazenar ou pegar seus pertences no momento que desejar, com a seguran&ccedil;a que procura.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Loca&ccedil;&atilde;o f&aacute;cil, r&aacute;pida e sem burocracia</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Total sigilo e privacidade: Ao alugar um dos nossos boxes, voc&ecirc; recebe a chave, tranca o box e a leva com voc&ecirc;. O acesso &eacute; s&oacute; seu!</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Seguran&ccedil;a 24h: Oferecemos monitoramento e seguran&ccedil;a ao seu box 24 horas por dia, sete dias da semana.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Self storage para a &aacute;rea m&eacute;dica &ndash; Cl&iacute;nicas e hospitais: Arquivos de prontu&aacute;rios antigos, aquisi&ccedil;&atilde;o de novos equipamentos e substitui&ccedil;&atilde;o de aparelhos, necessidade de maior circula&ccedil;&atilde;o em alas antigas ou novas.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Self storage para escrit&oacute;rios: A RV Self Storage a gente tem um lema: no escrit&oacute;rio s&oacute; o necess&aacute;rio. Isto porque o dia a dia de trabalho j&aacute; &eacute; agitado demais para voc&ecirc; ter que se preocupar com a pilha de processos que n&atilde;o deixa voc&ecirc; ver nem o funcion&aacute;rio &agrave; sua frente. &nbsp;Por isto, a&nbsp; RV self storage tem se tornado parceira de escrit&oacute;rios diversos. Advogados, contadores, administradores de empresas e gerentes de projetos j&aacute; contam com os boxes da&nbsp; RV self storage para reunir toda aquela papelada, documentos, processos e arquivo-morto e manter tudo organizado aqui.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Self storage para empreiteiros: Que tal aproveitar o self storage como solu&ccedil;&atilde;o de armazenamento para materiais de constru&ccedil;&atilde;o, equipamentos e at&eacute; projetos t&eacute;cnicos?</div>\r\n<div>\r\n	A RV Self Storage disponibiliza espa&ccedil;os especialmente projetados para armazenar aquilo que voc&ecirc; e a sua empreiteira precisam. Voc&ecirc; pode usar boxes self storage para armazenar itens do dia a dia de sua obra, ou tamb&eacute;m para guardar os bens de seus clientes enquanto a reforma est&aacute; em andamento.</div>\r\n<div>\r\n	O self storage pode tamb&eacute;m ajudar quando voc&ecirc; precisa locar equipamentos adicionais e n&atilde;o tem mais espa&ccedil;o na empresa para guardar. Ou mesmo para guardar os equipamentos que possui e que aluga para outros empreiteiros quando n&atilde;o est&aacute; fazendo uso.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Self storage para os servi&ccedil;os de hospitalidade: Hot&eacute;is, casas de festa, buffets e todos os profissionais especialistas em hospitalidade sabem da import&acirc;ncia em conservar seus m&oacute;veis e objetos de decora&ccedil;&atilde;o de maneira adequada. Afinal, durante uma celebra&ccedil;&atilde;o ou um per&iacute;odo de lazer e descanso, estes objetos ajudam a envolver os convidados no clima da festa. Nem passa pela cabe&ccedil;a dos convidados ou dos h&oacute;spedes o trabalho que d&aacute; para guardar candelabros, cadeiras, mesas, toalhas, lou&ccedil;a. O que eles querem, e voc&ecirc; tem que garantir, &eacute; a exibi&ccedil;&atilde;o de objetos com apar&ecirc;ncia sempre nova.</div>\r\n<div>\r\n	Aproveite as vantagens da RV self storage para hospitalidade, confira alguns dos itens que voc&ecirc; pode armazenar e guarde aqui as suas coisas: Cadeiras; Mesas; Pain&eacute;is; Lou&ccedil;as; Objetos diversos de decora&ccedil;&atilde;o (lustres, candelabros etc.).</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Self storage para neg&oacute;cios sazonais: Aqui voc&ecirc; pode priorizar a loca&ccedil;&atilde;o dos equipamentos para cada per&iacute;odo do ano e conservar corretamente o que faz do seu neg&oacute;cio um sucesso a cada nova esta&ccedil;&atilde;o!</div>\r\n<div>\r\n	Aproveite os benef&iacute;cios da RV self storage para servi&ccedil;os de loca&ccedil;&atilde;o sazonal ou tempor&aacute;ria e confira o que pode ser guardado: Jet skis; Kite surf boards; Snowboards; Skis; Stand up paddle; Caiaques; Equipamentos de mergulho; Artigos de praia; Artigos de camping.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	-Self storage para artes &ndash; Artistas, Colecionadores e Antiqu&aacute;rios: Voc&ecirc; pode aproveitar os benef&iacute;cios da RV self storage para artes e guardar suas obras e objetos com qualidade, seguran&ccedil;a, privacidade e sigilo total.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Temos boxes para armazenar pe&ccedil;as valiosas como as suas exigem. Apenas voc&ecirc; tem acesso ao box. Os contratos s&atilde;o mensais. O acesso ilimitado permite que voc&ecirc; recupere seus objetos sempre que a inspira&ccedil;&atilde;o aparecer.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Guarde tudo com seguran&ccedil;a, privacidade, e conforto!</div>', 'RV GUARDA MÓVEIS E SELF STORAGE BRASÍLIA DF', '', 'A RV Storage e Guarda Móveis DF,  possui o espaço ideal para pessoas e para empresas.', 'SIM', 0, 'locacao-de-box--self-storage', 0, NULL, NULL, NULL, NULL, NULL, NULL, '0806201711385333967183..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriaproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=83 ;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(1, '2304201608391132287293.jpg', 'SIM', NULL, NULL, 1),
(2, '2304201608391390468664.jpeg', 'SIM', NULL, NULL, 1),
(3, '2304201608391216243444.jpg', 'SIM', NULL, NULL, 1),
(4, '2304201608391286373039.jpg', 'SIM', NULL, NULL, 1),
(5, '2304201608391196362761.jpg', 'SIM', NULL, NULL, 1),
(6, '2304201608391257217307.jpeg', 'SIM', NULL, NULL, 1),
(7, '2304201608401214716216.jpg', 'SIM', NULL, NULL, 1),
(10, '2304201608401291559253.jpg', 'SIM', NULL, NULL, 1),
(11, '2304201608401328922935.jpg', 'SIM', NULL, NULL, 2),
(12, '2304201608401121401425.jpeg', 'SIM', NULL, NULL, 2),
(13, '2304201608401326053386.jpg', 'SIM', NULL, NULL, 7),
(14, '2304201608401266971012.jpeg', 'SIM', NULL, NULL, 7),
(15, '2304201608401342028212.jpg', 'SIM', NULL, NULL, 2),
(16, '2304201608401334137434.jpg', 'SIM', NULL, NULL, 7),
(17, '2304201608401299135454.jpg', 'SIM', NULL, NULL, 2),
(18, '2304201608401301585086.jpg', 'SIM', NULL, NULL, 7),
(19, '2304201608401164767362.jpg', 'SIM', NULL, NULL, 8),
(20, '2304201608401136114268.jpeg', 'SIM', NULL, NULL, 8),
(21, '2304201608401113833066.jpg', 'SIM', NULL, NULL, 2),
(22, '2304201608401256848592.jpg', 'SIM', NULL, NULL, 7),
(23, '2304201608401397319780.jpg', 'SIM', NULL, NULL, 8),
(24, '2304201608401170596735.jpeg', 'SIM', NULL, NULL, 2),
(25, '2304201608401398902206.jpeg', 'SIM', NULL, NULL, 7),
(26, '2304201608401351779362.jpg', 'SIM', NULL, NULL, 9),
(27, '2304201608401361983171.jpeg', 'SIM', NULL, NULL, 9),
(28, '2304201608401195748917.jpg', 'SIM', NULL, NULL, 8),
(29, '2304201608401343919274.jpg', 'SIM', NULL, NULL, 7),
(30, '2304201608401297893129.jpg', 'SIM', NULL, NULL, 2),
(31, '2304201608401299814369.jpg', 'SIM', NULL, NULL, 9),
(32, '2304201608401322948267.jpg', 'SIM', NULL, NULL, 7),
(33, '2304201608401350785028.jpg', 'SIM', NULL, NULL, 8),
(34, '2304201608401311382441.jpg', 'SIM', NULL, NULL, 2),
(35, '2304201608401400787798.jpg', 'SIM', NULL, NULL, 10),
(36, '2304201608401198274153.jpeg', 'SIM', NULL, NULL, 10),
(37, '2304201608401336804703.jpg', 'SIM', NULL, NULL, 9),
(38, '2304201608401359217121.jpeg', 'SIM', NULL, NULL, 8),
(39, '2304201608401168835486.jpg', 'SIM', NULL, NULL, 10),
(40, '2304201608411310709541.jpg', 'SIM', NULL, NULL, 8),
(41, '2304201608411368046935.jpg', 'SIM', NULL, NULL, 9),
(42, '2304201608411201570478.jpg', 'SIM', NULL, NULL, 11),
(43, '2304201608411120085087.jpeg', 'SIM', NULL, NULL, 11),
(44, '2304201608411250978247.jpg', 'SIM', NULL, NULL, 10),
(45, '2304201608411185044661.jpg', 'SIM', NULL, NULL, 8),
(46, '2304201608411270384475.jpeg', 'SIM', NULL, NULL, 9),
(47, '2304201608411255545885.jpg', 'SIM', NULL, NULL, 11),
(48, '2304201608411390762690.jpg', 'SIM', NULL, NULL, 10),
(49, '2304201608411129692391.jpg', 'SIM', NULL, NULL, 12),
(50, '2304201608411128176896.jpg', 'SIM', NULL, NULL, 9),
(51, '2304201608411232450966.jpeg', 'SIM', NULL, NULL, 12),
(52, '2304201608411112979030.jpg', 'SIM', NULL, NULL, 11),
(53, '2304201608411325840611.jpg', 'SIM', NULL, NULL, 9),
(54, '2304201608411246546794.jpeg', 'SIM', NULL, NULL, 10),
(55, '2304201608411182130584.jpg', 'SIM', NULL, NULL, 12),
(56, '2304201608411160079773.jpg', 'SIM', NULL, NULL, 10),
(57, '2304201608411286189121.jpg', 'SIM', NULL, NULL, 11),
(58, '2304201608411120380868.jpg', 'SIM', NULL, NULL, 13),
(59, '2304201608411308726899.jpeg', 'SIM', NULL, NULL, 13),
(60, '2304201608411219402158.jpg', 'SIM', NULL, NULL, 12),
(61, '2304201608411153269183.jpg', 'SIM', NULL, NULL, 10),
(62, '2304201608411114718367.jpeg', 'SIM', NULL, NULL, 11),
(63, '2304201608411138369015.jpg', 'SIM', NULL, NULL, 13),
(64, '2304201608411223154482.jpg', 'SIM', NULL, NULL, 11),
(65, '2304201608411177957868.jpg', 'SIM', NULL, NULL, 12),
(66, '2304201608411365556899.jpg', 'SIM', NULL, NULL, 14),
(67, '2304201608411293708919.jpg', 'SIM', NULL, NULL, 13),
(68, '2304201608411208373012.jpeg', 'SIM', NULL, NULL, 14),
(69, '2304201608411278302664.jpeg', 'SIM', NULL, NULL, 12),
(70, '2304201608411347220644.jpg', 'SIM', NULL, NULL, 11),
(71, '2304201608411216557624.jpg', 'SIM', NULL, NULL, 13),
(72, '2304201608411249469931.jpg', 'SIM', NULL, NULL, 12),
(73, '2304201608411165591436.jpg', 'SIM', NULL, NULL, 14),
(74, '2304201608411141598748.jpeg', 'SIM', NULL, NULL, 13),
(75, '2304201608411329651229.jpg', 'SIM', NULL, NULL, 14),
(76, '2304201608411394875887.jpg', 'SIM', NULL, NULL, 12),
(77, '2304201608411247300164.jpg', 'SIM', NULL, NULL, 13),
(78, '2304201608411306615434.jpg', 'SIM', NULL, NULL, 14),
(79, '2304201608411286060988.jpg', 'SIM', NULL, NULL, 13),
(80, '2304201608411400061668.jpeg', 'SIM', NULL, NULL, 14),
(81, '2304201608411291974774.jpg', 'SIM', NULL, NULL, 14),
(82, '2304201608411261315228.jpg', 'SIM', NULL, NULL, 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO',
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`, `super_admin`) VALUES
(1, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL, 'NAO'),
(2, 'Amanda', 'f83a356b993f4d2728f21bb2eddd3b58', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', 'amanda', 'SIM'),
(3, 'Junior', '9eee66d4cc448f9061af528b75941f0e', 'SIM', 0, 'aroj75@gmail.com', 'SIM', 'junior', 'NAO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=319 ;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:00:35', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:04:48', 1),
(3, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:07:05', 1),
(4, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:08:09', 1),
(5, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:29:16', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:36:51', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '23:43:19', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-03', '00:26:52', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-03', '12:55:52', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '13:49:39', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '22:40:32', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '22:43:25', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:23:17', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:23:48', 1),
(15, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '20:37:32', 1),
(16, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:48:58', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:53:28', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:09:54', 1),
(19, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:10:17', 1),
(20, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:10:38', 1),
(21, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:16:08', 1),
(22, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:17:58', 1),
(23, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '11:47:16', 1),
(24, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '15:03:28', 1),
(25, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '20:53:17', 1),
(26, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '12:39:07', 1),
(27, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:04:11', 1),
(28, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:06:54', 1),
(29, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:16:16', 1),
(30, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:16:48', 1),
(31, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:17:20', 1),
(32, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:19:58', 1),
(33, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:20:41', 1),
(34, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:26:01', 1),
(35, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:28:44', 1),
(36, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '21:46:41', 1),
(37, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '23:10:36', 1),
(38, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:46:07', 1),
(39, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:48:02', 1),
(40, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:48:32', 1),
(41, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:48:46', 1),
(42, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:50:54', 1),
(43, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:52:09', 1),
(44, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:53:23', 1),
(45, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:53:36', 1),
(46, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:55:04', 1),
(47, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:55:20', 1),
(48, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:55:34', 1),
(49, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:55:53', 1),
(50, 'DESATIVOU O LOGIN 44', 'UPDATE tb_servicos SET ativo = ''NAO'' WHERE idservico = ''44''', '2016-05-16', '12:55:58', 1),
(51, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:56:50', 1),
(52, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:57:10', 1),
(53, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:57:21', 1),
(54, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:57:40', 1),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:57:56', 1),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:58:16', 1),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:58:45', 1),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:59:00', 1),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '12:59:14', 1),
(60, 'DESATIVOU O LOGIN 14', 'UPDATE tb_equipamentos SET ativo = ''NAO'' WHERE idequipamento = ''14''', '2016-05-16', '12:59:19', 1),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:02:00', 1),
(62, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:02:10', 1),
(63, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:02:22', 1),
(64, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:02:32', 1),
(65, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:02:46', 1),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:02:54', 1),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:03:08', 1),
(68, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:03:16', 1),
(69, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:03:23', 1),
(70, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:03:31', 1),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:03:39', 1),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:03:57', 1),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:04:06', 1),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:04:18', 1),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:04:25', 1),
(76, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:04:31', 1),
(77, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:04:52', 1),
(78, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:05:01', 1),
(79, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:05:08', 1),
(80, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:05:21', 1),
(81, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:05:31', 1),
(82, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:05:39', 1),
(83, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:06:47', 1),
(84, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:06:54', 1),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:07:01', 1),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:07:07', 1),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:07:14', 1),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:07:20', 1),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:07:27', 1),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:07:34', 1),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:09:57', 1),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:10:07', 1),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:10:17', 1),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:10:25', 1),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:10:35', 1),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:10:43', 1),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:10:53', 1),
(98, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:11:02', 1),
(99, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '13:11:18', 1),
(100, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:22:01', 1),
(101, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:26:27', 1),
(102, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:27:32', 1),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:33:54', 1),
(104, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:38:25', 1),
(105, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:39:12', 1),
(106, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:44:33', 1),
(107, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:47:42', 1),
(108, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:55:39', 1),
(109, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '17:57:05', 1),
(110, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:04:10', 1),
(111, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:05:07', 1),
(112, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:05:31', 1),
(113, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:08:38', 1),
(114, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:09:19', 1),
(115, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:09:55', 1),
(116, 'DESATIVOU O LOGIN 11', 'UPDATE tb_equipamentos SET ativo = ''NAO'' WHERE idequipamento = ''11''', '2016-05-16', '18:19:12', 1),
(117, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:21:35', 1),
(118, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:22:56', 1),
(119, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:33:17', 1),
(120, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:34:42', 1),
(121, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:39:33', 1),
(122, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:40:14', 1),
(123, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:40:46', 1),
(124, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '18:41:20', 1),
(125, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '01:31:46', 1),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '01:39:26', 1),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '01:40:42', 1),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '01:41:53', 1),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '03:18:33', 1),
(130, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''44''', '2016-05-17', '03:23:59', 1),
(131, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''41''', '2016-05-17', '03:26:48', 1),
(132, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''40''', '2016-05-17', '03:26:51', 1),
(133, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''39''', '2016-05-17', '03:26:53', 1),
(134, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '10:28:00', 1),
(135, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '10:30:22', 1),
(136, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '12:29:48', 1),
(137, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '14:26:10', 1),
(138, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '14:51:15', 1),
(139, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '17:41:21', 1),
(140, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '17:47:29', 1),
(141, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '18:03:04', 1),
(142, 'DESATIVOU O LOGIN 37', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''37''', '2016-05-17', '19:28:45', 1),
(143, 'DESATIVOU O LOGIN 38', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''38''', '2016-05-17', '19:28:48', 1),
(144, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '20:45:53', 1),
(145, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '21:29:33', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '21:31:56', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '21:32:41', 1),
(148, 'CADASTRO DO CLIENTE ', '', '2016-05-17', '21:54:33', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '21:57:49', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '22:03:15', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '22:06:40', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '22:08:01', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '22:18:03', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '22:19:00', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '03:52:12', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '03:53:53', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '04:09:25', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '04:11:58', 1),
(159, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''4''', '2016-05-18', '04:19:00', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-05-18', '04:38:14', 1),
(161, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '04:40:13', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '04:48:48', 1),
(163, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '04:57:51', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '04:58:29', 1),
(165, 'CADASTRO DO CLIENTE ', '', '2016-05-18', '04:59:03', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '05:00:28', 1),
(167, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '05:04:32', 1),
(168, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '05:10:51', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '11:54:58', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '12:30:11', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '12:31:44', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '12:48:18', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:01:41', 1),
(174, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_equipamentos WHERE idequipamento = ''14''', '2016-05-18', '18:22:46', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '18:23:13', 1),
(176, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '18:38:00', 1),
(177, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '18:41:34', 1),
(178, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '18:56:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '02:45:32', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '02:50:12', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '02:55:18', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '03:35:35', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '03:36:33', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '03:44:30', 1),
(185, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '03:47:01', 1),
(186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '03:51:23', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '04:18:14', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '04:24:04', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '04:34:33', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '04:39:15', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '04:44:12', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '04:45:23', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '04:53:43', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '05:07:25', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:10:35', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:10:58', 1),
(197, 'ATIVOU O LOGIN 37', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''37''', '2016-05-19', '14:11:18', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:16:33', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:23:18', 1),
(200, 'ATIVOU O LOGIN 38', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''38''', '2016-05-19', '14:23:22', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:24:35', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '14:25:31', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '16:11:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '16:38:01', 1),
(205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '16:38:50', 1),
(206, 'CADASTRO DO LOGIN ', '', '2016-05-20', '11:18:39', 1),
(207, 'ALTERAÇÃO DO LOGIN 2', '', '2016-05-20', '11:29:07', 1),
(208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-20', '15:07:34', 1),
(209, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:44:35', 1),
(210, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:45:28', 1),
(211, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:46:12', 1),
(212, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:46:47', 1),
(213, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:47:27', 1),
(214, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:57:19', 1),
(215, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:58:43', 1),
(216, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '10:59:38', 1),
(217, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:53', 1),
(218, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:59', 1),
(219, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:05:27', 1),
(220, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:06:37', 1),
(221, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:07:23', 1),
(222, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:08:34', 1),
(223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:14:31', 1),
(224, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:16:37', 1),
(225, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:18:25', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:19:49', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:21:18', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:22:05', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:23:37', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:25:23', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:26:30', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:28:19', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:29:52', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:32:10', 1),
(235, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:34:04', 1),
(236, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:19:18', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '13:00:53', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '13:06:08', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '13:06:34', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '14:11:53', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '18:17:04', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '18:18:00', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '18:18:26', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '18:52:10', 2),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:54:46', 1),
(246, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:55:45', 1),
(247, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:56:10', 1),
(248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:56:58', 1),
(249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:59:24', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:59:51', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '11:00:49', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '11:01:43', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '11:03:28', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '11:22:35', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '11:25:10', 1),
(256, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '16:09:50', 1),
(257, 'CADASTRO DO CLIENTE ', '', '2017-06-08', '11:35:53', 1),
(258, 'CADASTRO DO CLIENTE ', '', '2017-06-08', '11:38:09', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2017-06-08', '11:39:46', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-03', '11:41:27', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-05', '13:59:41', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:15:19', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:46:06', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:46:42', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:47:43', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:48:41', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:49:08', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:50:40', 1),
(269, 'ALTERAÇÃO DO CLIENTE ', '', '2017-07-10', '15:54:22', 1),
(270, 'ALTERAÇÃO DO CLIENTE ', '', '2018-01-17', '15:54:16', 1),
(271, 'ALTERAÇÃO DO CLIENTE ', '', '2018-01-17', '15:56:38', 1),
(272, 'ALTERAÇÃO DO CLIENTE ', '', '2018-01-17', '16:00:47', 1),
(273, 'ALTERAÇÃO DO CLIENTE ', '', '2018-01-17', '16:02:04', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:07:44', 2),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:08:05', 2),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:08:19', 2),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:08:33', 2),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:17:17', 2),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:17:34', 2),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:18:39', 2),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:19:09', 2),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:19:32', 2),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:19:49', 2),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:20:04', 2),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:20:28', 2),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:20:53', 2),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:22:16', 2),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:22:40', 2),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:23:06', 2),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:24:20', 2),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:24:38', 2),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:25:02', 2),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:25:29', 2),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:26:05', 2),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:27:55', 2),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:28:52', 2),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:29:05', 2),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:29:20', 2),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:31:35', 2),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:31:53', 2),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:32:42', 2),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:33:14', 2),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:33:38', 2),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-17', '16:34:46', 2),
(305, 'CADASTRO DO LOGIN ', '', '2018-07-17', '16:37:30', 2),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '03:49:25', 3),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '03:52:49', 3),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '03:55:26', 3),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '03:58:47', 3),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '04:01:00', 3),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '04:05:21', 3),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '04:30:39', 3),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '04:33:09', 3),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '04:41:59', 3),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2018-07-18', '04:46:23', 3),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2018-08-28', '17:15:33', 2),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2018-08-28', '17:16:36', 2),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2018-08-28', '17:17:12', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`) VALUES
(1, 'Produto 1 Lorem ipsum dolor sit amet', '1904201602441205856083..jpg', '<p>\r\n	Produto 1&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'SIM', 0, 'produto-1-lorem-ipsum-dolor-sit-amet', 80, '3M', NULL, NULL, '', '', 1),
(2, 'Produto 2 Lorem ipsum dolor sit amet', '2104201604421266314038..jpg', '<p>\r\n	Porta postigo direita Viena Ouro&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>\r\n<p>\r\n	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'SIM', 0, 'produto-2-lorem-ipsum-dolor-sit-amet', 77, 'Tigre', NULL, NULL, '', '', 3),
(7, 'Produto 3 Lorem ipsum dolor sit amet', '2104201604411406395471..jpg', '<p>\r\n	Fogos de Artif&iacute;cios 0&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'SIM', 0, 'produto-3-lorem-ipsum-dolor-sit-amet', 82, 'Amanco', NULL, NULL, '', '', 4),
(8, 'Produto 4 Lorem ipsum dolor sit amet', '2104201604451260634326..jpg', '<p>\r\n	4&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-4-lorem-ipsum-dolor-sit-amet', 80, 'Sol', NULL, NULL, '', '', 1),
(9, 'Produto 5 Lorem ipsum dolor sit amet', '2104201604451352705126.jpeg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-5-lorem-ipsum-dolor-sit-amet', 81, NULL, NULL, NULL, '', '', NULL),
(10, 'Produto 6 Lorem ipsum dolor sit amet', '2104201604451351836739.jpeg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-6-lorem-ipsum-dolor-sit-amet', 77, NULL, NULL, NULL, '', '', NULL),
(11, 'Produto 7 Lorem ipsum dolor sit amet', '2104201604461305784289..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-7-lorem-ipsum-dolor-sit-amet', 77, NULL, NULL, NULL, 'https://www.youtube.com/embed/xIM22tZAdwE', '<p>\r\n	Apresenta&ccedil;&atilde;o do v&iacute;deo Lorem Ipsum is simply dummy text of the printing and typesettin g industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a gall ey of type and scrambled it to make a type specimen book. It h as survived not only five centuries, but alsothe leap into electro nic typesetting, remaining essentially unchanged. It was popularis ed in the 1960</p>', NULL),
(12, 'Produto 8 Lorem ipsum dolor sit amet', '2104201606431150666300..jpg', '<p>\r\n	Produto 8 Lorem ipsum dolor sit amet&nbsp;Produto 8 Lorem ipsum dolor sit amet&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-8-lorem-ipsum-dolor-sit-amet', 75, NULL, NULL, NULL, '', '', NULL),
(13, 'Produto 9 Lorem ipsum dolor sit amet', '2104201606431365068955..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-9-lorem-ipsum-dolor-sit-amet', 74, NULL, NULL, NULL, '', '', NULL),
(14, 'Produto 1001 Lorem ipsum dolor sit amet', '2304201607531280757694..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-1001-lorem-ipsum-dolor-sit-amet', 82, NULL, NULL, NULL, '', '', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE IF NOT EXISTS `tb_seo` (
  `idseo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idseo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'RV Transportes Locações de Guindastes DF - Aluguel de Guindastes, Aluguel de Caminhões Munck, Aluguel de Carretas, Locação de Empilhadeiras, Locação de Paleteiras, Locação de Containers Maritimos DF.', 'A RV Locação de Guindastes DF, oferece Locação e Aluguel de Guindastes, Caminhão Munck, Carretas, Empilhadeiras, Aluguel de Containers Marítimos e toda a linha de Guinchos para Içamentos e Guindastes para Construção Civil com atendimento em Brasília no DF e Rio Verde - GO.', 'Locação de Guindastes DF, Aluguel de Guindastes DF, Locação de Guindastes Brasília DF, Aluguel de Guindastes Brasília DF, Locação de Muncks, Aluguel de Munck, Aluguel de Caminhão Munck DF, Locação de Guindastes Rio Verde Goiás, Aluguel de Guindastes Rio Verde Goiás, Aluguel de Munck Rio Verde Goiás, Locação de Munck Rio Verde Goiás, Aluguel de Carretas DF, Locação de Carretas DF, Aluguel de Carretas Rio Verde Goiás, Locação de Carretas Rio Verde Goiás, Locação de Empilhadeiras DF, Aluguel de Empilhadeiras DF, Locação de Empilhadeiras Rio Verde Goiás, Aluguel de Empilhadeiras Rio Verde Goiás, Aluguel de Container DF, Locação de Container DF, Aluguel de Container Marítimos DF, Locação de Container Marítimos DF, Aluguel de Conteiner Marítimos DF, Aluguel de Container Marítimos Rio Verde Goias, Aluguel de Container Marítimos Goias, Aluguel de Conteiners Marítimos DF, Locação de Paleteiras DF, Aluguel de Paleteiras DF, Aluguel de Paleteiras Rio Verde Goiás, Locação de Paleteiras Rio Verde DF.', 'SIM', NULL, 'index'),
(2, 'Empresa', 'RV Locações de Guindastes DF - Aluguel e Locação de Guindastes, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers.', 'A RV  Locação de Guindastes DF, oferece Locação e Aluguel de Guindastes, Caminhão Munck, Carretas, Empilhadeiras, Aluguel de Containers Marítimos e toda a linha de Guinchos para Içamentos e Guindastes para Construção Civil com atendimento em Brasília no DF e Rio Verde - GO.', 'Locação de Guindastes DF, Aluguel de Guindastes DF, Locação de Guindastes Brasília DF, Aluguel de Guindastes Brasília DF, Locação de Muncks, Aluguel de Munck, Aluguel de Caminhão Munck DF, Locação de Guindastes Rio Verde Goiás, Aluguel de Guindastes Rio Verde Goiás, Aluguel de Munck Rio Verde Goiás, Locação de Munck Rio Verde Goiás, Aluguel de Carretas DF, Locação de Carretas DF, Aluguel de Carretas Rio Verde Goiás, Locação de Carretas Rio Verde Goiás, Locação de Empilhadeiras DF, Aluguel de Empilhadeiras DF, Locação de Empilhadeiras Rio Verde Goiás, Aluguel de Empilhadeiras Rio Verde Goiás, Aluguel de Container DF, Locação de Container DF, Aluguel de Container Marítimos DF, Locação de Container Marítimos DF, Aluguel de Conteiner Marítimos DF, Aluguel de Container Marítimos Rio Verde Goias, Aluguel de Container Marítimos Goias, Aluguel de Conteiners Marítimos DF, Locação de Paleteiras DF, Aluguel de Paleteiras DF, Aluguel de Paleteiras Rio Verde Goiás, Locação de Paleteiras Rio Verde DF.', 'SIM', NULL, 'empresa'),
(3, 'Serviços', 'RV  Locações e Aluguel Guindates DF , Locação de Carretas, Locação de Caminhões Munck, Locação de Empilhadeiras, Paleteiras, Locação de Containers.', 'A RV  Locação de Guindastes DF, oferece Locação e Aluguel de Guindastes, Caminhão Munck, Carretas, Empilhadeiras, Aluguel de Containers Marítimos e toda a linha de Guinchos para Içamentos e Guindastes para Construção Civil com atendimento em Brasília no DF e Rio Verde - GO.', 'Locação de Guindastes DF, Aluguel de Guindastes DF, Locação de Guindastes Brasília DF, Aluguel de Guindastes Brasília DF, Locação de Muncks, Aluguel de Munck, Aluguel de Caminhão Munck DF, Locação de Guindastes Rio Verde Goiás, Aluguel de Guindastes Rio Verde Goiás, Aluguel de Munck Rio Verde Goiás, Locação de Munck Rio Verde Goiás, Aluguel de Carretas DF, Locação de Carretas DF, Aluguel de Carretas Rio Verde Goiás, Locação de Carretas Rio Verde Goiás, Locação de Empilhadeiras DF, Aluguel de Empilhadeiras DF, Locação de Empilhadeiras Rio Verde Goiás, Aluguel de Empilhadeiras Rio Verde Goiás, Aluguel de Container DF, Locação de Container DF, Aluguel de Container Marítimos DF, Locação de Container Marítimos DF, Aluguel de Conteiner Marítimos DF, Aluguel de Container Marítimos Rio Verde Goias, Aluguel de Container Marítimos Goias, Aluguel de Conteiners Marítimos DF, Locação de Paleteiras DF, Aluguel de Paleteiras DF, Aluguel de Paleteiras Rio Verde Goiás, Locação de Paleteiras Rio Verde DF.', 'SIM', NULL, 'servicos'),
(4, 'Equipamentos', 'RV  Locações e Aluguel Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília, Rio Verde Goiás.', 'A RV  Transportes oferece aluguel e locação de Guindastes, Equipamentos para Içamentos e Muncks em Brasília DF e Rio Verde Goiás.', 'Locação de Equipamentos para Içamentos DF, Içamentos DF, Locação de Guindastes DF, Aluguel de Guindastes, Aluguel de Muncks, Aluguel de Equipamento para Içamentos, Aluguel de Guincho para Içamento DF, Locação de Equipamentos para Içamentos Rio Verde Goiás, Içamentos Goiás, Locação de Guindastes Rio Verde Goiás, Aluguel de Guindastes Rio Verde Goiás, Aluguel de Muncks Rio Verde Goiás, Aluguel de Equipamento para Içamentos Rio Verde Goiás, Aluguel de Guincho para Içamentos Rio Verde Goiás.', 'SIM', NULL, 'equipamentos'),
(5, 'Dicas', 'CPC Transportes Locação de Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília - DF', 'Dicas para Escolha de Locação de Guindastes', '', 'SIM', NULL, 'dicas'),
(6, 'Contatos', 'CPC Transportes Locação de Guindates DF - CONTATO', 'CPC Transportes Locação de Guindates DF - CONTATO', '', 'SIM', NULL, 'contatos'),
(7, 'Trabalhe conosco', 'CPC Locações DE Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília DF, Rio Verde Goiás.', 'CPC Transportes Locação de Guindates DF - TRABALHE CONOSCO', 'CPC Transportes Locação de Guindates DF - TRABALHE CONOSCO', 'SIM', NULL, 'trabalhe-conosco'),
(8, 'Orçamentos', 'CPC Transportes Locações de Guindates, Carretas, Caminhões Munck, Empilhadeiras, Paleteiras, Containers - Brasília DF, Rio Verde Goiás', 'CPC Transportes Locação de Guindates - ORÇAMENTOS', 'Orçamento de Aluguel de Guindastes DF, Orçamento de Locação de Guindastes DF, Orçamento de Aluguel de Munck DF, Orçamento de Locação de Guindastes DF, Orçamento de Aluguel de Guindastes Rio Verde Goiás, Orçamento de Aluguel de Guindastes Goiás, Orçamento de Locação de Guindastes Rio verde Goiás, Orçamento de Aluguel de Caminhão Munck Rio Verde Goiás, Orçamento de Içamentos DF, Orçamento de Içamentos Rio Verde Goiás, Orçamento de Içamentos, Orçamento de Aluguel de Containers DF, Orçamento de Locação de Container DF, Orçamento de Locação de Container Rio Verde Goiás, Orçamento de Locação de Container, Orçamento Aluguel de Container Rio Verde Goiás, Orçamento de Locação de Empilhadeiras, Orçamento de Locação de Empilhadeiras DF, Orçamento de Locação de Empilhadeiras Rio Verde Goiás, Orçamento de Locação de Carretas DF, Orçamento de Locação de Carretas Rio Verde Goiás, Orçamento de Aluguel de Carretas DF, Orçamento de Aluguel de Carretas Rio Verde Goiás.', 'SIM', NULL, 'orcamentos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idservico`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=46 ;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`) VALUES
(40, 'REMOÇÃO TÉCNICA', '<div style="text-align: justify;">\r\n	Equipe especializada para a execu&ccedil;&atilde;o de remo&ccedil;&otilde;es t&eacute;cnicas de m&aacute;quinas e equipamentos, mudan&ccedil;a de layout industrial. A empresa possui Know-How na movimenta&ccedil;&atilde;o e transporte de: M&aacute;quinas Injetoras, Grupo Gerador, M&aacute;quinas Operatrizes, Prensas e Tornos, dentre outras.</div>', '1805201611543066320431..jpg', 'SIM', 0, 'remocao-tecnica', 'RV Transportes - Remoção Técnica DF, Remoção Técnica Rio verde DF.', '', 'Equipe especializada para a execução de remoções técnicas de máquinas e equipamentos, mudança de layout industrial em Brasília - DF e Rio Verde Goiás.', 0, '', '', '2104201610131175693451.png'),
(41, 'IÇAMENTOS', '<div style="text-align: justify;">\r\n	Com a RV Guindastes e Transportes voc&ecirc; ou sua empresa pode garantir i&ccedil;amentos de produtos e equipamentos em pr&eacute;dios comerciais e residenciais. Tratando caso a caso com cuidado e t&eacute;cnica que gere seguran&ccedil;a ao ambiente e certeza da entrega no ponto planejado.</div>', '1807201804468032277019..jpg', 'SIM', 0, 'icamentos', 'RV Transportes - Içamentos DF, Içamentos Rio Verde Goiás.', '', 'Com a RV Guindastes e Transportes você ou sua empresa pode garantir Içamentos em Brasília DF ou Rio Verde Goiás, de produtos e equipamentos em prédios comerciais e residenciais. Tratando caso a caso com cuidado e técnica que gere segurança ao ambiente e c', 0, '', '', '2104201610131348432865.png'),
(42, 'CARGA E DESCARGA', '<div style="text-align: justify;">\r\n	Execu&ccedil;&atilde;o de servi&ccedil;os de carga e descarga com a utiliza&ccedil;&atilde;o de caminh&otilde;es guindauto (munck) ou guindastes. A CPC utiliza todos os equipamentos de apoio necess&aacute;rios para que as opera&ccedil;&otilde;es sejam feitas com seguran&ccedil;a, e sem danos &agrave; carga. A capacidade dos guindautos (munck) varia de 12.000 KGF at&eacute; 40.000 KGF.</div>', '1805201612488720350882..jpg', 'SIM', 0, 'carga-e-descarga', 'RV Guindastes e Transportes Transportes - Carga e Descarga com Guindastes DF, Carga e Descarga com Guindastes Rio Verde Goiás.', '', 'Execução de serviços de carga e descarga com a utilização de caminhões guindauto (munck) ou guindastes em Brasília - DF e Rio Verde Goiás. A RV Guindastes e Transportes utiliza todos os equipamentos de apoio necessários para que as operações sejam feitas ', 0, '', '', '2104201610131240709875.png'),
(43, 'IÇAMENTO COM CESTA AÉREA', '<p>\r\n	I&ccedil;amento de pessoas com a utiliza&ccedil;&atilde;o de cesta a&eacute;rea acoplada em caminh&otilde;es guindauto (munck) ou guindastes para diversas finalidades, dentre elas:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Inspe&ccedil;&atilde;o de viadutos.</p>\r\n<p>\r\n	- Montagem e manuten&ccedil;&atilde;o de Comunica&ccedil;&atilde;o Visual totens, fachadas, front light, back light e luminosos.</p>\r\n<p>\r\n	- Inspe&ccedil;&atilde;o e aux&iacute;lio na montagem de estruturas met&aacute;licas.</p>\r\n<p>\r\n	- Aux&iacute;lio para filmagens de eventos.</p>\r\n<p>\r\n	- Manuten&ccedil;&atilde;o de m&aacute;quinas.</p>\r\n<p>\r\n	- Servi&ccedil;os diversos para constru&ccedil;&atilde;o civil.</p>', '1805201612307015798563..jpg', 'SIM', 0, 'icamento-com-cesta-aerea', 'RV Guindastes e Transportes Transportes - Içamentos com Cesta Aérea DF, Içamentos com Cesta Aérea Rio Verde Goiás.', '', 'Içamento de pessoas com a utilização de cesta aérea acoplada em caminhões guindauto (munck) ou guindastes para diversas finalidades em Brasília - DF e Rio Verde Goiás.', 0, '', '', '2104201610141153328479.png'),
(45, 'GUARDA MÓVEIS E SELF STORAGE', '<p>\r\n	A RV Storage e Guarda M&oacute;veis DF,&nbsp; possui o espa&ccedil;o ideal para pessoas e para empresas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	Seja para guardar m&oacute;veis, mudan&ccedil;as, volumes, arquivos, documentos, estoque ou o que voc&ecirc; precisar. Boxes exclusivos, de acordo com suas necessidades.<br />\r\n	A RV Self Storage voc&ecirc; pode armazenar ou pegar seus pertences no momento que desejar, com a seguran&ccedil;a que procura.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Loca&ccedil;&atilde;o f&aacute;cil, r&aacute;pida e sem burocracia</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	-Total sigilo e privacidade: Ao alugar um dos nossos boxes, voc&ecirc; recebe a chave, tranca o box e a leva com voc&ecirc;. O acesso &eacute; s&oacute; seu!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Seguran&ccedil;a 24h: Oferecemos monitoramento e seguran&ccedil;a ao seu box 24 horas por dia, sete dias da semana.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Self storage para a &aacute;rea m&eacute;dica &ndash; Cl&iacute;nicas e hospitais: Arquivos de prontu&aacute;rios antigos, aquisi&ccedil;&atilde;o de novos equipamentos e substitui&ccedil;&atilde;o de aparelhos, necessidade de maior circula&ccedil;&atilde;o em alas antigas ou novas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Self storage para escrit&oacute;rios: A RV Self Storage a gente tem um lema: no escrit&oacute;rio s&oacute; o necess&aacute;rio. Isto porque o dia a dia de trabalho j&aacute; &eacute; agitado demais para voc&ecirc; ter que se preocupar com a pilha de processos que n&atilde;o deixa voc&ecirc; ver nem o funcion&aacute;rio &agrave; sua frente.&nbsp; Por isto, a&nbsp; RV self storage tem se tornado parceira de escrit&oacute;rios diversos. Advogados, contadores, administradores de empresas e gerentes de projetos j&aacute; contam com os boxes da RV self storage para reunir toda aquela papelada, documentos, processos e arquivo-morto e manter tudo organizado aqui.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Self storage para empreiteiros: Que tal aproveitar o self storage como solu&ccedil;&atilde;o de armazenamento para materiais de constru&ccedil;&atilde;o, equipamentos e at&eacute; projetos t&eacute;cnicos?<br />\r\n	A RV Self Storage disponibiliza espa&ccedil;os especialmente projetados para armazenar aquilo que voc&ecirc; e a sua empreiteira precisam. Voc&ecirc; pode usar boxes self storage para armazenar itens do dia a dia de sua obra, ou tamb&eacute;m para guardar os bens de seus clientes enquanto a reforma est&aacute; em andamento.</p>\r\n<p>\r\n	O self storage pode tamb&eacute;m ajudar quando voc&ecirc; precisa locar equipamentos adicionais e n&atilde;o tem mais espa&ccedil;o na empresa para guardar. Ou mesmo para guardar os equipamentos que possui e que aluga para outros empreiteiros quando n&atilde;o est&aacute; fazendo uso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Self storage para os servi&ccedil;os de hospitalidade: Hot&eacute;is, casas de festa, buffets e todos os profissionais especialistas em hospitalidade sabem da import&acirc;ncia em conservar seus m&oacute;veis e objetos de decora&ccedil;&atilde;o de maneira adequada. Afinal, durante uma celebra&ccedil;&atilde;o ou um per&iacute;odo de lazer e descanso, estes objetos ajudam a envolver os convidados no clima da festa. Nem passa pela cabe&ccedil;a dos convidados ou dos h&oacute;spedes o trabalho que d&aacute; para guardar candelabros, cadeiras, mesas, toalhas, lou&ccedil;a. O que eles querem, e voc&ecirc; tem que garantir, &eacute; a exibi&ccedil;&atilde;o de objetos com apar&ecirc;ncia sempre nova.</p>\r\n<p>\r\n	Aproveite as vantagens da RV self storage para hospitalidade, confira alguns dos itens que voc&ecirc; pode armazenar e guarde aqui as suas coisas: Cadeiras; Mesas; Pain&eacute;is; Lou&ccedil;as; Objetos diversos de decora&ccedil;&atilde;o (lustres, candelabros etc.).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Self storage para neg&oacute;cios sazonais: Aqui voc&ecirc; pode priorizar a loca&ccedil;&atilde;o dos equipamentos para cada per&iacute;odo do ano e conservar corretamente o que faz do seu neg&oacute;cio um sucesso a cada nova esta&ccedil;&atilde;o!</p>\r\n<p>\r\n	Aproveite os benef&iacute;cios da RV self storage para servi&ccedil;os de loca&ccedil;&atilde;o sazonal ou tempor&aacute;ria e confira o que pode ser guardado: Jet skis; Kite surf boards; Snowboards; Skis; Stand up paddle; Caiaques; Equipamentos de mergulho; Artigos de praia; Artigos de camping.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<br />\r\n	-Self storage para artes &ndash; Artistas, Colecionadores e Antiqu&aacute;rios: Voc&ecirc; pode aproveitar os benef&iacute;cios da RV self storage para artes e guardar suas obras e objetos com qualidade, seguran&ccedil;a, privacidade e sigilo total.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Temos boxes para armazenar pe&ccedil;as valiosas como as suas exigem. Apenas voc&ecirc; tem acesso ao box. Os contratos s&atilde;o mensais. O acesso ilimitado permite que voc&ecirc; recupere seus objetos sempre que a inspira&ccedil;&atilde;o aparecer.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Guarde tudo com seguran&ccedil;a, privacidade, e conforto!</p>', '0806201711353354345961..jpg', 'SIM', NULL, 'guarda-moveis-e-self-storage', 'RV GUARDA MÓVEIS E SELF STORAGE BRASÍLIA DF', 'storage, self storage, guarda móveis, guarda documentos, box guarda móveis, box aluguel, aluguel de box para guardar móveis, storage DF, self storage DF, guarda móveis DF, guarda documentos DF, box guarda móveis DF, box aluguel DF, aluguel de box para gua', 'A RV Storage e Guarda Móveis DF,  possui o espaço ideal para pessoas e para empresas. Seja para guardar móveis, mudanças, volumes, arquivos, documentos, estoque ou o que você precisar. Boxes exclusivos, de acordo com suas necessidades.', 0, '', '', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
