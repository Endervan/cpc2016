<!-- colocando topo sobre slider home -->
<div class="topo-site">

	<!--  ==============================================================  -->
	<!-- contatos -->
	<!--  ==============================================================  -->
	<div class="container-fluid fundo-cinza-grandiente pb10">
		<div class="row">
			<div class="container">
				<div class="row">

					<!-- contatos -->
					<div class="col-xs-9 text-left top5 tel-topo">
						<div class="pull-right left10">
							<h4><i class="fa fa-phone-square right10"></i> <?php Util::imprime($config[telefone1]); ?> <span><?php Util::imprime($config[telefone1_local]); ?></span> </h4>
							<h4><i class="fa fa-phone-square right10"></i> <?php Util::imprime($config[telefone2]); ?> <span><?php Util::imprime($config[telefone2_local]); ?></span></h4>
						</div>
						

					</div>
					<!-- contatos -->



					<!-- carrinho -->
					<div class="col-xs-3 text-right">
						<!-- botao com 2 linhas texto-->
						<a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="tam-btn-topo btn">

							<i class="right10 pull-left"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-carrinho.png" alt=""></i>
							<span class="top20">MEU ORÇAMENTO</span>
							<i class="fa fa-caret-down left10" aria-hidden="true"></i>
						</a>
						<!-- botao com 2 linhas texto-->
						<div class="dropdown-menu topo-meu-orcamento pull-right">

							<h6 class="bottom20">MEU ORÇAMENTO(<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h6>


							<?php
				            if(count($_SESSION[solicitacoes_produtos]) > 0)
				            {
				                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
				                {
				                    $row = $obj_site->select_unico("tb_equipamentos", "idequipamento", $_SESSION[solicitacoes_produtos][$i]);
				                    ?>
				                    <div class="lista-itens-carrinho">
				                        <div class="col-xs-2">
				                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem_interna]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
				                        </div>
				                        <div class="col-xs-8">
				                            <h1><?php Util::imprime($row[titulo]) ?></h1>
				                        </div>
				                        <div class="col-xs-1">
				                            <a href="<?php echo Util::caminho_projeto() ?>/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
				                        </div>
				                    </div>
				                    <?php
				                }
				            }
				            ?>


				            <?php
				            if(count($_SESSION[solicitacoes_servicos]) > 0)
				            {
				                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
				                {
				                    $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
				                    ?>
				                    <div class="lista-itens-carrinho">
				                        <div class="col-xs-2">
				                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
				                        </div>
				                        <div class="col-xs-8">
				                            <h1><?php Util::imprime($row[titulo]) ?></h1>
				                        </div>
				                        <div class="col-xs-1">
				                            <a href="<?php echo Util::caminho_projeto() ?>/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
				                        </div>
				                    </div>
				                    <?php
				                }
				            }
				            ?>


							<div class="text-right bottom20" >
								<a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-vermelho" >
									FINALIZAR
								</a>
							</div>

						</div>
					</div>
					<!-- carrinho -->
				</div>
			</div>
		</div>
	</div>
	<!--  ==============================================================  -->
	<!-- contatos -->
	<!--  ==============================================================  -->





	<!--  ==============================================================  -->
	<!-- menu topo -->
	<!--  ==============================================================  -->
	<div class="container-fluid fundo-vermelho">
		<div class="row">
			<div class="container">
				<div class="row">
					<!-- logo -->
					<div class="col-xs-2 posicao-logo">
						<a href="<?php echo Util::caminho_projeto() ?>/" title="">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
						</a>		
					</div>
					<!-- logo -->

					<!--  ==============================================================  -->
					<!-- menu diagonal -->
					<!--  ==============================================================  -->
					<div class="col-xs-8 menu-topo padding0">
						<header class="group">
							<nav>
								<ul>
									<li class="active"><a href="<?php echo Util::caminho_projeto() ?>/"><span><i class="fa fa-home" aria-hidden="true"></i></span></a></li>
									<li class=""><a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a></li>
									<li class=""><a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>
									<li class=""><a href="<?php echo Util::caminho_projeto() ?>/equipamentos">EQUIPAMENTOS</a></li>
									<li class=""><a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
									<li class=""><a href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS</a></li>
								</ul>
							</nav>
						</header>
						
					</div>
					<!-- area restrita -->
					<div class="col-xs-2 text-right">
						<a class="btn btn-restrito" href="#" role="button">
							<br><i class="fa fa-lock fa-2x" aria-hidden="true"></i><br>
							<span>ÁREA RESTRITA</span></a>
						</div>
						<!-- area restrita -->
					</div>
				</div>
			</div>
		</div>
		<!--  ==============================================================  -->
		<!-- menu topo -->
		<!--  ==============================================================  -->


	</div>
