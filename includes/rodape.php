<div class="clearfix"></div>
<div class="container-fluid rodape">
	<div class="row">

		<!-- menu -->
		<div class="container top30">
			<div class="row">
				<div class="col-xs-12 bg-menu-rodape">
					<ul class="menu-rodape">
						<li class="active"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/equipamentos">EQUIPAMENTOS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/area-restrita">ÁREA RESTRITA</a></li>
					</ul>
				</div>

				<!-- menu -->

				<!-- logo, endereco, telefone -->
				<div class="col-xs-3 top10">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-rodape.png" alt="">
					</a>
				</div>

				<div class="col-xs-7 top30">
					<div class="bottom15">
						<p><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco]); ?></p>

						<?php if (!empty($config[endereco_2])) { ?>
							<p><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco_2]); ?></p>
						<?php } ?>
					</div>
					<div class="top20 bottom15">
						<p><i class="glyphicon glyphicon-phone"></i> 
							<?php Util::imprime($config[telefone1]); ?> <span><?php Util::imprime($config[telefone1_local]); ?>
							

							<?php if (!empty($config[telefone2])) { ?>
								 / <?php Util::imprime($config[telefone2]); ?> <span><?php Util::imprime($config[telefone2_local]); ?>
							<?php } ?>

							<?php if (!empty($config[telefone3])) { ?>
								 / <?php Util::imprime($config[telefone3]); ?> <span><?php Util::imprime($config[telefone3_local]); ?>
							<?php } ?>

							<?php if (!empty($config[telefone4])) { ?>
								 / <?php Util::imprime($config[telefone4]); ?> <span><?php Util::imprime($config[telefone4_local]); ?>
							<?php } ?>
						</p>
					</div>
				</div>



				<div class="col-xs-2 top30 text-right">
					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<i class="fa fa-google-plus right15" style="color: #000"></i>
						</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
					</a>
				</div>	
				<!-- logo, endereco, telefone -->

			</div>
		</div>
	</div>
</div>


<div class="container-fluid rodape-preto">
	<div class="row">
		<div class="col-xs-12 text-center top15 bottom15">
			<h5>© Copyright CPC LOCAÇÃO</h5>
		</div>
	</div>
</div>


