
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row top30 bottom50">
    
    <?php if(!empty($config[menu_3])): ?>
        <div class="col-4">
            <div class="">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/equipamentos">
                    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_equipamentos.png"
                            width="95"
                            height="95"
                            layout="responsive"
                            alt="AMP">
                    </amp-img>
                </a>
            </div>   
        </div>
    <?php endif; ?>


    
        <div class="col-4">
            <div class="">
                    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
                    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos.png"
                            width="95"
                            height="95"
                            layout="responsive"
                            alt="AMP">
                    </amp-img>
                </a>
            </div>  
        </div>
    

    <?php if(!empty($config[menu_6])): ?>
    <div class="col-4 ">
        <div class="">
        <a 
                on="tap:my-lightbox24horas"
                role="a"
                tabindex="0">
                    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contato.png"
                        width="95"
                        height="95"
                        layout="responsive"
                        alt="AMP">
                    </amp-img>
            </a>
        </div>     
    </div>
    <?php endif; ?>
    
    
</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->

