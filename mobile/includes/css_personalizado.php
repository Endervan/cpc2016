

body{
    background: <?php echo $config[background] ?> ;
}



/* TODOS BOTÕES AZUIS DO SITE */
.btn_1, .btn_1:hover,
.desc-prod h2,
.lista-categorias li:hover, .lista-categorias li.destaque,
a.btn_1
{
    background: <?php echo $config[btn_1_bg] ?> ;
    color: <?php echo $config[btn_1_color] ?>;
    border-radius: 0px;
    border: 1px solid <?php echo $config[btn_1_border] ?>;
}
/* TODOS BOTÕES AZUIS DO SITE */



/* TODOS BOTÕES TRANSPARENTES DO SITE */
.btn_2, .btn_2:hover,
a.btn_2{
    background: <?php echo $config[btn_2_bg] ?> ;
    color: <?php echo $config[btn_2_color] ?>;
    border-radius: 0px;
    border: 1px solid <?php echo $config[btn_2_border] ?>;
}

.btn_2_pequeno,
a.btn_2_pequeno{
    border: 1px solid <?php echo $config[btn_2_border] ?>;
    font-size: 10px;
}
/* TODOS BOTÕES TRANSPARENTES DO SITE */


/* TEXTOS DO SITE */
h1{color:<?php echo $config[texto_h1_h2_h3] ?>;font-size:25px;}
h1 span{font-size:20px;color:<?php echo $config[texto_h1_h2_h3] ?>;}
h2{color:<?php echo $config[texto_h1_h2_h3] ?>;font-weight:700;font-size:35px;}
h3{font-size:32px;color:<?php echo $config[texto_h1_h2_h3] ?>;}
h3 span{font-size:43px;color:<?php echo $config[texto_h1_h2_h3] ?>;font-weight:700;}
.FormCurriculo label{color:<?php echo $config[texto_h1_h2_h3] ?>;}

.cor_preco_carrinho{color:<?php echo $config[texto_h1_h2_h3] ?>}

/* TEXTOS DO SITE */



.menu_mobile_mt{
    <?php if(!empty($config[menu_mobile_mt])){ echo 'margin-top:'.$config[menu_mobile_mt].'px;';  } ?>
}

.seta_voltar_mobile_mt{
    <?php if(!empty($config[seta_voltar_mobile_mt])){ echo 'margin-top:'.$config[seta_voltar_mobile_mt].'px;';  } ?>
}

.br10{border-radius:10px;}