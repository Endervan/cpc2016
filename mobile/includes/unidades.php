<!-- ======================================================================= -->
  <!-- mapa   -->
  <!-- ======================================================================= -->
  <div class="row top5 relativo">
      <div class="col-12 padding0  relativo">
          
      <?php
        //  busca os produtos sem filtro
        $result = $obj_site->select("tb_unidades");
        if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
            ?>
                <div class="lista-unidades">

                    <div class="col-12 text-center titulo-unidade">
                        <h4><?php Util::imprime($row[titulo]); ?></h4>
                    </div>

                    <div class="col-12 top10">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-home media-object right10" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><?php Util::imprime($row[endereco]);?></h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 top20">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <a href="tel:+55<?php Util::imprime($row[ddd1]);?> <?php Util::imprime($row[telefone1]);?>">
                                    <h6 class="media-heading"><span><?php Util::imprime($row[ddd1]);?> <?php Util::imprime($row[telefone1]);?></span></h6>
                                </a>
                            </div>
                        </div>
                    </div>

                    <?php if (!empty($row[telefone2])) :?>
                        <div class="col-6 top20">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="fa fa-whatsapp media-object right10" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <a href="tel:+55<?php Util::imprime($row[ddd2]);?> <?php Util::imprime($row[telefone2]);?>">
                                        <h6 class="media-heading"><span><?php Util::imprime($row[ddd2]);?> <?php Util::imprime($row[telefone2]);?></span></h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($row[telefone3])) :?>
                        <div class="col-6 top20">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <a href="tel:+55<?php Util::imprime($row[ddd3]);?> <?php Util::imprime($row[telefone3]);?>">
                                        <h6 class="media-heading"><span><?php Util::imprime($row[ddd3]);?> <?php Util::imprime($row[telefone3]);?></span></h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($row[telefone4]) != '') :?>
                        <div class="col-6 top20">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <a href="tel:+55<?php Util::imprime($row[ddd1]);?> <?php Util::imprime($row[telefone1]);?>">
                                        <h5 class="media-heading"><span><?php Util::imprime($row[ddd4]);?> <?php Util::imprime($row[telefone4]);?></span></h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    

                    <div class="col-12 top20">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-clock-o media-object right10" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><span><?php Util::imprime($row[horario_funcionamento]);?></span></h6>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="clearfix"></div>
                    
                </div>

                <?php if(!empty($row[src_place])): ?>
                <amp-iframe
                            width="320"
                            height="199"
                            layout="responsive"
                            sandbox="allow-scripts allow-same-origin allow-popups"
                            frameborder="0"
                            src="<?php Util::imprime($row[src_place]); ?>">
                </amp-iframe>
                <?php endif; ?>
                
                <div class="top30"></div>

            <?php
            }
        }
        ?>
          


      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- mapa   -->
  <!-- ======================================================================= -->