<?php
header('AMP-Access-Control-Allow-Source-Origin: http://localhost');
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_carrinho = new Carrinho();


?>


<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>

  


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  <?php require_once("../includes/css_personalizado.php");  //  CSS PERSONALIZADO ?>
  <?php require_once("../includes/css_personalizado.php");  //  CSS PERSONALIZADO ?>

  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  </style>


  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

  
</head>

<body class="bg-interna">

    <?php  require_once("../includes/topo.php")?>


    <div class="row">
        <div class="col-12  top20 text-center">
            <?php
            if(isset($_GET[tipo_pagamento])){
                
                echo $obj_carrinho->finaliza_venda($_GET[tipo_pagamento]);
                
                if($_GET[tipo_pagamento] == 'maquineta'){
                    header("location: index.php?enviado=sim");
                }
                
            }
            ?>
        </div>
    </div>


  <?php require_once("../includes/rodape.php") ?>

</body>



</html>