<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>



  <!-- FlexSlider 2 -->
  <script type="text/javascript" >
   $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: false,
      itemWidth: 113,
      itemMargin: 1
    });
  });
</script>
<!-- FlexSlider 2 -->

</head>

<body>

  <?php require_once('./includes/topo.php'); ?>

<!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 2 order by ordem");
                 if(mysql_num_rows($result) > 0){
                  $i = 0;
                   while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                   ?> 
                      <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                   <?php 
                    $i++;
                   }
                }
                ?>
              </ol>

              <!-- Controls -->
              <a class="left carousel-control setas-home" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control setas-home" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>
          </div>
        </div> 
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  --> 
        

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          


  
          <?php 
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
            ?>
            <div class="item <?php if($i == 0){ echo "active"; } ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="">
              <div class="carousel-caption">

                <div class="container">
                  <div class="row">
                    <div class="col-xs-12 text-left">
                      <h5><?php Util::imprime($imagem[titulo]); ?></h5>
                      <p><span><?php Util::imprime($imagem[legenda]); ?> </span></p>
                      <div class="col-xs-5 top20 padding0">
                        <div class="top20">
                          <?php if (!empty($imagem[url])): ?>
                            <a class="btn btn-transparente" href="<?php Util::imprime($imagem[url]); ?>"  title="SOLICITE UM ORÇAMENTO">SOLICITE UM ORÇAMENTO</a>
                          <?php endif; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <?php 
            $i++;
            }
          }
          ?>


        </div>


      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!-- PRODUTOS HOME-->
  <!--  ==============================================================  -->
  <div class="container fundo-cinza">
    <div class="row produtos-home pb40 top40">
      <div id="Carousel" class="carousel slide">

       

        <!-- Carousel items -->
        <div class="carousel-inner">

          <div class="item active">
            

            <?php
            $result = $obj_site->select("tb_equipamentos", "order by rand() limit 2");
             if(mysql_num_rows($result) > 0){
              $i = 0;
               while ($row = mysql_fetch_array($result)) {
               ?> 
                  <div class="col-xs-12 posicao top20">
                    <div class="col-xs-10 fundo-cinza pb40">
                     <div class="top30">
                      <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
                    </div>
                    <div class="top20 pb40 col-xs-12">
                      <p><?php Util::imprime($row[descricao], 300); ?></p>
                      <span class="pull-right">...</span>  
                    </div>
                    <a class="btn btn-transparente-produtos top20" href="<?php echo Util::caminho_projeto() ?>/mobile/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS">
                      SAIBA MAIS
                    </a>
                  </div>
                  <div class="posicao-imagem-produtos">
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" width="253" height="164" >
                    </a>
                  </div>
                  </div>
                <?php 
                }
              }
              ?>
            

      </div><!--.item-->

        

        
      </div><!--.carousel-inner-->
  
      </div><!--.Carousel-->
  </div>
</div><!--.container-->
<!--  ==============================================================  -->
<!-- PRODUTOS HOME-->
<!--  ==============================================================  --> 



<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 
<div class="container fundo-vermelho-grandiente pb60">
  <div class="row top20">

    <div class="col-xs-12">
      <div class="text-right top10">
        <img src=" <?php echo Util::caminho_projeto() ?>/mobile/imgs/descricao-conheca.png" alt="">
      </div>
      <div class="top20"> 
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
              <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
       <div class="top10">
          <a class="btn btn-transparente-conheca " href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" role="button">SAIBA MAIS</a>
       </div>
       <div class="top10">
          <a class="btn btn-transparente-conheca " href="<?php echo Util::caminho_projeto() ?>/mobile/contatos" role="button">FALE CONOSCO</a>
       </div>
    </div>

    <div class="col-xs-12 top20">
      <div class="pull-right left10">
          <a href="tel+55<?php Util::imprime($config[telefone1]); ?>">
            <h4><?php Util::imprime($config[telefone1]); ?></h4>
          </a>
        </div>
        <div class="pull-right">
          <h3><i class="fa fa-phone-square right10"></i>ATENDIMENTO:</h3>
        </div>
        <div class="top15">
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 

<!--  ==============================================================  -->
<!-- SERVICOS HOME-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row servicos-home">
    <div class="col-xs-4 padding0">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/servicos-home.png" alt="">
    </div>

    <div class="col-xs-8 top50">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/servicos-home-titulo.png" alt="">
      

      <?php
      $result = $obj_site->select("tb_servicos", "order by rand() limit 3");
       if(mysql_num_rows($result) > 0){
        $i = 0;
         while ($row = mysql_fetch_array($result)) {
         ?> 
            <div class="media top15">
              <div class="media-left media-middle">
                  <i class="fa fa-star" aria-hidden="true" class="media-object"></i>
              </div>
              <div class="media-body">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <p class="media-heading"><?php Util::imprime($row[titulo]); ?></p>
                </a>
              </div>
            </div>
         <?php 
         }
       }
       ?>

    </div>

  </div>
</div> 
<!--  ==============================================================  -->
<!-- SERVICOS HOME-->
<!--  ==============================================================  --> 


<!--  ==============================================================  -->
<!-- DICAS HOME-->
<!--  ==============================================================  -->
<div class="container fundo-cinza pb40">
  <div class="row">

    
    <!--  ==============================================================  -->
    <!-- ITEM 01-->
    <!--  ==============================================================  -->
    <?php
    $result = $obj_site->select("tb_dicas", "order by rand() limit 2");
     if(mysql_num_rows($result) > 0){
      $i = 0;
       while ($row = mysql_fetch_array($result)) {
       ?> 
        <div class="text-center top25">
          <div class="col-xs-6">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 225, 206, array("class"=>"", "alt"=>"$row[titulo]")) ?>
              <div class="borda-interna"></div>
              </a>
              </div>
              <div class="col-xs-6 dicas-home">
                <div class="top20 pb10">
                  <p><?php Util::imprime($row[titulo]); ?></p>
                </div>
              <a class="btn btn-transparente-conheca top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS">SAIBA MAIS</a>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php 
        }
      }
      ?>

  </div>
</div> 
<!--  ==============================================================  -->
<!-- DICAS HOME-->
<!--  ==============================================================  --> 






<?php require_once('./includes/rodape.php'); ?>





</body>

</html>


