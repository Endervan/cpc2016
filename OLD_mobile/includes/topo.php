


<!--  ==============================================================  -->
<!-- telefones topo-->
<!--  ==============================================================  -->
<div class="container fundo-cinza-grandiente ">
  <div class="row">
    <!-- contatos -->
    <div class="col-xs-12 top1">

      <div class="pull-right">
        <a class="btn btn-preto" href="tel:+55<?php Util::imprime($config[telefone1]); ?>" role="button">CHAMAR</a>

        <div class="top1">
          <a class="btn btn-preto" href="tel:+55<?php Util::imprime($config[telefone2]); ?>" role="button">CHAMAR</a>  
        </div>
        
      </div>

      <div class="pull-right right10 top1">
        <h4><i class="fa fa-phone-square right10"></i> <?php Util::imprime($config[telefone1]); ?> <span><?php Util::imprime($config[telefone1_local]); ?></span> </h4>
        <h4 class="top15"><i class="fa fa-phone-square right10"></i> <?php Util::imprime($config[telefone2]); ?> <span><?php Util::imprime($config[telefone2_local]); ?></span></h4>
      </div>


    </div>
    <!-- contatos -->
  </div>
</div> 
<!--  ==============================================================  -->
<!-- telefones topo-->
<!--  ==============================================================  --> 



<!--  ==============================================================  -->
<!-- menu-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <div class="col-xs-12 text-center fundo-cinza top5 pt5 pb5">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
      </a>
    </div>

    <div class="col-xs-12 menu-topo">
      <!-- menu-topo -->
      <div class="col-xs-5 padding0">
        <div class=" dropdown top10">
          <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-menu">
            <i class="fa fa-bars right15"></i> 
            NOSSO MENU
            <i class="fa fa-chevron-down left15"></i>
          </a>
          <ul class="dropdown-menu sub-menu" aria-labelledby="dLabel">
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVIÇOS</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/equipamentos">EQUIPAMENTOS</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos">CONTATOS</a></li>
          </ul>
        </div>        
      </div>
      <!-- menu-topo -->


      <!-- ======================================================================= -->
      <!-- botao carrinho de compra -->
      <!-- ======================================================================= -->
      <div class="col-xs-offset-1 col-xs-3 dropdown text-right padding0">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" class="btn btn_transparente" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-carrinho.png" alt="">
        </a>

        <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

         <?php
         if(count($_SESSION[solicitacoes_produtos]) > 0)
         {
          
          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
          {
            $row = $obj_site->select_unico("tb_equipamentos", "idequipamento", $_SESSION[solicitacoes_produtos][$i]);
            ?>
            <div class="lista-itens-carrinho col-xs-12">
              <div class="col-xs-2">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem_interna]", 46, 29, array("class"=>"", "alt"=>"")) ?>
              </div>
              <div class="col-xs-8">
                <h1><?php Util::imprime($row[titulo]) ?></h1>
              </div>
              <div class="col-xs-1">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
              </div>
            </div>
            <?php  
          }
        }

        ?>

        <?php

        if(count($_SESSION[solicitacoes_servicos]) > 0)
        {


          for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
          {
            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
            ?>
            <div class="lista-itens-carrinho col-xs-12">
              <div class="col-xs-2">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
              </div>
              <div class="col-xs-8">
                <h1><?php Util::imprime($row[titulo]) ?></h1>
              </div>
              <div class="col-xs-1">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
              </div>
            </div>
            <?php  
          }
        }

        ?>

        <div class="text-right bottom20">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" title="Finalizar" class="btn btn-formulario" >
            <h3>FINALIZAR</h3>
          </a>
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- botao carrinho de compra -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- botao restrito -->
    <!-- ======================================================================= -->
    <div class=" col-xs-3 text-right">
      <a href="#" class="btn btn_transparente">
        <i class="fa fa-lock" aria-hidden="true"></i>
      </a>
    </div>
    <!-- ======================================================================= -->
    <!-- botao restrito -->
    <!-- ======================================================================= -->

  </div>
</div>
</div> 
<!--  ==============================================================  -->
<!-- menu-->
<!--  ==============================================================  --> 



<!--  ==============================================================  -->
<!-- menu-->
<!--  ==============================================================  -->

<!-- menu-topo -->



