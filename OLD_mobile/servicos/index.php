<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();



// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>


<?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
<style>
  .bg-interna-imagem{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
  }
</style>


<body>
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 


  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->
  <div class="container bg-interna-imagem bg-alt-internas">
    <div class="row titulo-internos">
      <div class="col-xs-12 top35 text-center">
        <h1>CONHEÇA NOSSOS</h1>
        <h2>SERVIÇOS</h2>
      </div>

      <!--  ==============================================================  -->
      <!-- barra pesquisas -->
      <!--  ==============================================================  -->
      <form action="<?php echo Util::caminho_projeto() ?>/mobile/servicos/" method="post">
        <div class="col-xs-offset-2 col-xs-8 top5">
          <div class="input-group stylish-input-group">
            <input type="text" class="form-control" name="busca_servicos"  placeholder="PESQUISAR" >
            <span class="input-group-addon">
              <button type="submit">
                <span class="glyphicon glyphicon-search"></span>
              </button>  
            </span>
          </div>
        </div>
      </form>
      <!--  ==============================================================  -->
      <!-- barra pesquisas -->
      <!--  ==============================================================  -->

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->




  <!--  ==============================================================  -->
  <!-- SERVICOS DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container fundo-cinza">
    <div class="row subir-dicas bottom50">



      <?php
      if(isset($_POST[busca_equipamentos])):
        $complemento = "and titulo LIKE '%$_POST[busca_servicos]%'";
      endif;

      $result = $obj_site->select("tb_servicos", $complemento);
      if(mysql_num_rows($result) == 0){
        echo '
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <h6 class="top50">
                      <span>Nenhum registro encontrado.</span>
                    </h6>          
                  </div>
                </div>
              </div>
              ';
      }else{ 
         $i = 0;
         while ($row = mysql_fetch_array($result)) {
         ?>
            <div class="col-xs-12 bottom40">
              <div class="servico_img">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 450, 322, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  <div class="borda_servico"></div>
                </a>
                <div class="top25">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>
              </div>

              <div class="top15 servicos_gerais">
                <p><?php Util::imprime($row[descricao], 400); ?></p>
              </div>
              <div class="col-xs-8 top30 padding0">
                <a class="btn btn-transparente-servicos" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                  SOLICITE UM ORÇAMENTO
                </a>
              </div>
              <div class="col-xs-4 top30 padding0">
                <a class="btn btn-transparente-servicos" href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS">SAIBA MAIS</a>
              </div>
            </div>
         <?php 
         }
       }
       ?>










    </div>
  </div> 




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>
