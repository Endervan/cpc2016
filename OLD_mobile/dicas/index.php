<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13) ?>
<style>
  .bg-interna-imagem{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
  }
</style>

<body>
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!-- bg-dicas -->
  <!--  ==============================================================  -->
  <div class="container bg-interna-imagem">
    <div class="row titulo-internos">
      <div class="col-xs-12 top35 text-center">
        <h1>CONHEÇA NOSSAS</h1>
        <h2>DICAS</h2>
      </div>

      <!--  ==============================================================  -->
      <!-- barra pesquisas -->
      <!--  ==============================================================  -->
      <form action="<?php echo Util::caminho_projeto() ?>/mobile/dicas/" method="post">
        <div class="col-xs-offset-2 col-xs-8 top5">
          <div class="input-group stylish-input-group">
            <input type="text" class="form-control" name="busca_equipamentos" placeholder="PESQUISAR" >
            <span class="input-group-addon">
              <button type="submit">
                <span class="glyphicon glyphicon-search"></span>
              </button>  
            </span>
          </div>
        </div>
      </form>

      <!--  ==============================================================  -->
      <!-- barra pesquisas -->
      <!--  ==============================================================  -->
      
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-dicas -->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!-- DICAS HOME-->
  <!--  ==============================================================  -->
  <div class="container fundo-cinza pb40">
    <div class="row">


      <!--  ==============================================================  -->
      <!-- ITEM 01-->
      <!--  ==============================================================  -->
      <?php
      if(isset($_POST[busca_equipamentos])):
        $complemento = "and titulo LIKE '%$_POST[busca_equipamentos]%'";
      endif;

      $result = $obj_site->select("tb_dicas", $complemento);
      if(mysql_num_rows($result) == 0){
        echo '
        <h6 class="top40">
          <span>Nenhum registro encontrado.</span>
        </h6>
        ';
      }else{ 
       $i = 0;
       while ($row = mysql_fetch_array($result)) {
         ?>
         <div class="text-center subir ">
          <div class="col-xs-6 top20">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 230, 206, array("class"=>"", "alt"=>"$row[titulo]")) ?>
              <div class="borda-interna"></div>
            </a>
          </div>
          <div class="col-xs-6 top20 dicas-home">
            <div class="top20 pb10 pr20">
              <p><?php Util::imprime($row[titulo]); ?></p>
            </div>
            <a class="btn btn-transparente-conheca top20 bottom20" href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
          </div>
        </div>

        <?php 
        if ($i == 2) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }

      }
    }
    ?>
    




  </div>
</div> 
<!--  ==============================================================  -->
<!-- DICAS HOME-->
<!--  ==============================================================  --> 






<?php require_once('../includes/rodape.php'); ?>

</body>

</html>