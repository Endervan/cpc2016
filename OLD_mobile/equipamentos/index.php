<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>


<?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11) ?>
<style>
  .bg-interna-imagem{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
  }
</style>


<body>
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 


  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->
  <div class="container bg-interna-imagem bg-alt-internas">
    <div class="row titulo-internos">
      <div class="col-xs-12 top35 text-center">
        <h1>CONHEÇA NOSSOS</h1>
        <h2>EQUIPAMENTOS</h2>
      </div>

      <!--  ==============================================================  -->
      <!-- barra pesquisas -->
      <!--  ==============================================================  -->
      <form action="<?php echo Util::caminho_projeto() ?>/mobile/equipamentos/" method="post">
        <div class="col-xs-offset-2 col-xs-8 top5">
          <div class="input-group stylish-input-group">
            <input type="text" class="form-control" name="busca_equipamentos"  placeholder="PESQUISAR" >
            <span class="input-group-addon">
              <button type="submit">
                <span class="glyphicon glyphicon-search"></span>
              </button>  
            </span>
          </div>
        </div>
      </form>
      <!--  ==============================================================  -->
      <!-- barra pesquisas -->
      <!--  ==============================================================  -->

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!-- PRODUTOS-->
  <!--  ==============================================================  -->
  <div class="container fundo-cinza">
    <div class="row produtos-home pb60 top10">

      <!--  ==============================================================  -->
      <!-- ITEM 01-->
      <!--  ==============================================================  -->
      <?php
      if(isset($_POST[busca_equipamentos])):
        $complemento = "and titulo LIKE '%$_POST[busca_equipamentos]%'";
      endif;

      $result = $obj_site->select("tb_equipamentos", $complemento);
      if(mysql_num_rows($result) == 0){
        echo '
        <h6 class="top50">
          <span>Nenhum registro encontrado.</span>
        </h6>
        ';
      }else{ 
       $i = 0;
       while ($row = mysql_fetch_array($result)) {
         ?>
         <div class="col-xs-12 posicao top30">
          <div class="col-xs-10 fundo-cinza pb40">
           <div class="top30">
            <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
          </div>
          <div class="top20 pb40 col-xs-12">
            <p><?php Util::imprime($row[descricao], 300); ?></p>
            <span class="pull-right">...</span>  
          </div>
          <a class="btn btn-transparente-produtos top20" href="<?php echo Util::caminho_projeto() ?>/mobile/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" role="button">SAIBA MAIS</a>
        </div>
        <div class="posicao-imagem-produtos">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/equipamento/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" width="260" height="168" alt="<?php Util::imprime($row[titulo]); ?>">
        </a>
      </div>
    </div>

    <?php 
    if ($i == 1) {
      echo '<div class="clearfix"></div>';
      $i = 0;
    }else{
      $i++;
    }

  }
}
?>
</div>
</div>
<!--  ==============================================================  -->
<!-- PRODUTOS-->
<!--  ==============================================================  --> 








<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
