<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>


<?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
    .bg-interna-imagem{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
    }
</style>


<body>
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
<!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 


  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->
  <div class="container bg-interna-imagem bg-alt-internas">
    <div class="row titulo-internos">
      <div class="col-xs-12 top35 text-center">
        <h1>SEMPRE QUE PRECISAR</h1>
        <h2>FALE CONOSCO</h2>

        <!-- link entre paginas -->
        <div class="col-xs-6 text-left top110">
          <a class="btn btn-fale active" href="" role="button">FALE CONOSCO</a>
        </div>

        <div class="col-xs-6 text-left top110">
          <a class="btn btn-trabalhe" href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco" role="button">TRABALHE CONOSCO</a>
        </div>
        <!-- link entre paginas -->

      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- CONTATOS E FORMULARIO-->
  <!--  ==============================================================  -->
  <div class="container fundo-cinza pb50">
    <div class="row ">

    <!-- ======================================================================= -->
    <!-- contatos  -->
    <!-- ======================================================================= -->
    <?php require_once("../includes/dados_contato.php"); ?>
    <!-- ======================================================================= -->
    <!-- contatos  -->
    <!-- ======================================================================= -->

    <?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome])){



        //  ENVIANDO A MENSAGEM PARA O CLIENTE
        $texto_mensagem = "
           O seguinte cliente fez uma solicitação pelo site. <br />

           Nome: $_POST[nome] <br />
           Email: $_POST[email] <br />
           Telefone: $_POST[telefone] <br />
           Assunto:$_POST[assunto] <br />
           Mensagem: <br />
           ". nl2br($_POST[mensagem]) ." <br />

           ";
           

           Util::envia_email($config[email], utf8_decode("$_POST[nome]"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
           Util::envia_email($config[email_copia], utf8_decode("$_POST[nome]"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
           Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

 }
 ?>


      <div class="col-xs-12">
        <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data"> 
          <div class="fundo-formulario">
            <!-- formulario orcamento -->
            
              <div class="col-xs-12 top20">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                  <span class="fa fa-user form-control-feedback top10"></span>        
                </div>
              </div>
 
              <div class="col-xs-12 top20">
                <div class="form-group input100 has-feedback">
                  <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                  <span class="fa fa-envelope form-control-feedback top10"></span>        
                </div>
              </div>



              <div class="col-xs-12 top20">
                <div class="form-group input100 has-feedback">
                  <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                  <span class="fa fa-phone form-control-feedback top10"></span>        
                </div>
              </div>

              <div class="col-xs-12 top20">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                  <span class="fa fa-star form-control-feedback top10"></span>        
                </div>
              </div>

            <div class="clearfix"></div>  



            <div class="top15">
              <div class="col-xs-12 top20">        
               <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="30" rows="8" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback top10"></span> 
              </div>
            </div>
          </div>

          <!-- formulario orcamento -->
          <div class="col-xs-12 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn-vermelho-grande" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>
        <!--  ==============================================================  -->
        <!-- FORMULARIO-->
        <!--  ==============================================================  -->
      </form>

      <!--  ==============================================================  -->
      <!--COMO CHEGAR-->
      <!--  ==============================================================  -->
      <div class="contatos">
        <h6 class="top30"><span>COMO CHEGAR</span></h6>
      </div>
      <!--  ==============================================================  -->
      <!--COMO CHEGAR-->
      <!--  ==============================================================  -->

    </div>



  </div>
</div>
<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    
    <!-- Nav tabs -->
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center ul-maps">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php Util::imprime($config[src_place_local]); ?></a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php Util::imprime($config[src_place_local_1]); ?></a></li>
              </ul>          
          </div>
        </div>
      </div>
      

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <iframe src="<?php Util::imprime($config[src_place_2]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>

  </div>
</div>
<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- CONTATOS E FORMULARIO-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>

