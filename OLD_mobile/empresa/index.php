<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<body>
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!-- bg-empresa -->
  <!--  ==============================================================  -->
  <div class="container bg-empresa">
    <div class="row titulo-internos">
      <div class="col-xs-12 top35 text-center">
        <h1>CONHEÇA NOSSA</h1>
        <h2>EMPRESA</h2>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-empresa -->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!-- empresa descricao-->
  <!--  ==============================================================  -->
  <div class="container fundo-cinza">
   <div class="row empresa-descricao">
     <div class="col-xs-5 text-center top50">
       <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/empresa-descricao.png" alt="">
     </div>
     <div class="col-xs-7">
       <div class="top15 scroll">
         <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
           <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

    </div>
  </div>
</div> 
<!--  ==============================================================  -->
<!-- empresa descricao-->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- categoria descricao-->
<!--  ==============================================================  -->
<div class="container">
 <div class="row top15 bottom50">

   <div class="col-xs-6 text-center top15">
    <div class="categoria-descricao">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/equipamentos" title="EQUIPAMENTOS DE ALTA QUALIDADE">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/categoria-empresa01.png" alt="EQUIPAMENTOS DE ALTA QUALIDADE">
        <h1>EQUIPAMENTOS DE ALTA QUALIDADE</h1>
      </a>
    </div>
  </div>

  <div class="col-xs-6 text-center top15">
    <div class="categoria-descricao">
     <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos" title="SERVIÇOS ESPECIALIZADOS">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/categoria-empresa02.png" alt="SERVIÇOS ESPECIALIZADOS">
      <h1>SERVIÇOS ESPECIALIZADOS</h1>
    </a>
  </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-6 text-center top15">
  <div class="categoria-descricao">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos" title="ATENDIMENTO ESPECIALIZADO">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/categoria-empresa03.png" alt="ATENDIMENTO ESPECIALIZADO">
      <h1>ATENDIMENTO ESPECIALIZADO</h1>
    </a>
  </div>
</div>


</div>
</div>
<!--  ==============================================================  -->
<!-- categoria descricao-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 
<div class="container fundo-vermelho-grandiente-empresa pb40">
  <div class="row">



  <!--  ==============================================================  -->
  <!--maps-->
  <!--  ==============================================================  -->
    <div class="col-xs-12 top15">
      <div class="pull-right left10">
        <h4><?php Util::imprime($config[telefone1]); ?></h4>
      </div>
      <div class="pull-right">
        <h3><i class="fa fa-phone-square right10"></i>ATENDIMENTO:</h3>
      </div>
      <div class="clearfix"></div>
      <div class="top25">
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
    <!--  ==============================================================  -->
  <!--maps-->
  <!--  ==============================================================  -->
  </div>
</div>
<!--  ==============================================================  -->
<!-- CONHECA MAIS-->
<!--  ==============================================================  --> 


<?php require_once('../includes/rodape.php'); ?>

</body>

</html>