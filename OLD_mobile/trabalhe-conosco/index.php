<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>


<?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
    .bg-interna-imagem{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>) top center no-repeat;
    }
</style>


<body>
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
<!--  ==============================================================  -->
  <!-- background -->
  <!--  ==============================================================  --> 


  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->
  <div class="container bg-interna-imagem bg-alt-internas">
    <div class="row titulo-internos">
      <div class="col-xs-12 top35 text-center">
        <h1>SEMPRE QUE PRECISAR</h1>
        <h2>FALE CONOSCO</h2>

        <!-- link entre paginas -->
        <div class="col-xs-6 text-left top110">
          <a class="btn btn-fale" href="<?php echo Util::caminho_projeto() ?>/mobile/contatos" role="button">FALE CONOSCO</a>
        </div>

        <div class="col-xs-6 text-left top110">
          <a class="btn btn-trabalhe active" href="" role="button">TRABALHE CONOSCO</a>
        </div>
        <!-- link entre paginas -->

      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-fale conosco -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- CONTATOS E FORMULARIO-->
  <!--  ==============================================================  -->
  <div class="container fundo-cinza pb50">
    <div class="row ">

    <!-- ======================================================================= -->
    <!-- contatos  -->
    <!-- ======================================================================= -->
    <?php require_once("../includes/dados_contato.php"); ?>
    <!-- ======================================================================= -->
    <!-- contatos  -->
    <!-- ======================================================================= -->

    
    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {
      
      if(!empty($_FILES[curriculo][name])):
        $nome_arquivo = Util::upload_arquivo("../../uploads", $_FILES[curriculo]);
        $texto = "Anexo: ";
        $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
        $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
      endif;

          $texto_mensagem = "
                              Nome: ".$_POST[nome]." <br />
                              Telefone: ".$_POST[telefone]." <br />
                              Email: ".$_POST[email]." <br />
                              Escolaridade: ".$_POST[escolaridade]." <br />
                              Cargo: ".$_POST[cargo]." <br />
                              Área: ".$_POST[area]." <br />
                              Cidade: ".$_POST[cidade]." <br />
                              Estado: ".$_POST[estado]." <br />
                              Mensagem: <br />
                              ".nl2br($_POST[mensagem])."

                              <br><br>
                              $texto    
                              ";

            Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
            Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
    }
    ?>


      <div class="col-xs-12">
        <form class="form-inline FormContato" role="form" method="post" enctype="multipart/form-data"> 
          <div class="fundo-formulario">
            <!-- formulario orcamento -->
            
              <div class="col-xs-12 top20">
                <div class="form-group  input100 has-feedback">
                  <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                  <span class="fa fa-user form-control-feedback top10"></span>        
                </div>
              </div>

              <div class="col-xs-12 top20">
                <div class="form-group input100 has-feedback">
                  <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                  <span class="fa fa-phone form-control-feedback top10"></span>        
                </div>
              </div>

 
              <div class="col-xs-12 top20">
                <div class="form-group input100 has-feedback">
                  <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                  <span class="fa fa-envelope form-control-feedback top10"></span>        
                </div>
              </div>

              <div class="col-xs-12 top20">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
                    <span class="glyphicon glyphicon-briefcase form-control-feedback top5"></span>        
                  </div>
                </div>


                <div class="col-xs-12 top20">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
                    <span class="glyphicon glyphicon-globe form-control-feedback top5"></span>        
                  </div>
                </div>

                <div class="col-xs-12 top20">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
                    <span class="glyphicon glyphicon-globe form-control-feedback top5"></span>        
                  </div>
                </div>

                <div class="col-xs-12 top20">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
                    <span class="glyphicon glyphicon-book form-control-feedback top5"></span>        
                  </div>
                </div>


                <div class="col-xs-12 top20">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
                    <span class="glyphicon glyphicon-lock form-control-feedback top5"></span>        
                  </div>
                </div>

                <div class="col-xs-12 top20">
                  <div class="form-group input100 has-feedback ">
                    <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                    <span class="glyphicon glyphicon-file form-control-feedback"></span>        
                  </div>
                </div>

              <div class="col-xs-12 top20">        
               <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="30" rows="8" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback top10"></span> 
              </div>
            </div>
          <!-- formulario orcamento -->

          <div class="col-xs-12 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn-vermelho-grande" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>
        <!--  ==============================================================  -->
        <!-- FORMULARIO-->
        <!--  ==============================================================  -->
      </form>

      <!--  ==============================================================  -->
      <!--COMO CHEGAR-->
      <!--  ==============================================================  -->
      <div class="contatos">
        <h6 class="top30"><span>COMO CHEGAR</span></h6>
      </div>
      <!--  ==============================================================  -->
      <!--COMO CHEGAR-->
      <!--  ==============================================================  -->

    </div>



  </div>
</div>
<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <!-- Nav tabs -->
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center ul-maps">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?php Util::imprime($config[src_place_local]); ?></a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php Util::imprime($config[src_place_local_1]); ?></a></li>
              </ul>          
          </div>
        </div>
      </div>
      

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <iframe src="<?php Util::imprime($config[src_place_2]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- CONTATOS E FORMULARIO-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>